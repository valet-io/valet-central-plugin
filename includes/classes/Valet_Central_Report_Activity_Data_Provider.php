<?php
/**
 * Class Valet_Central_Report_Route
 */

use Carbon\Carbon;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

final class Valet_Central_Report_Activity_Data_Provider {

	private $maintained_site_id;
	private $from_date_in_gmt;
	private $to_date_in_gmt;
	private $gmt_offset_in_seconds;

	private $images_url;

	private static $all_res = null;
	private static $wh = null;
	private static $update_data_cache = null;
	private static $install_data_cache = null;
	private static $uninstall_data_cache = null;
	private static $activate_data_cache = null;
	private static $deactivate_data_cache = null;
	private static $user_action_data_cache = null;

	public function __construct( $maintained_site_id, $from_date_in_gmt, $to_date_in_gmt, $gmt_offset_in_seconds ) {
		$this->maintained_site_id    = intval( $maintained_site_id );
		$this->from_date_in_gmt      = $from_date_in_gmt;
		$this->to_date_in_gmt        = $to_date_in_gmt;
		$this->gmt_offset_in_seconds = $gmt_offset_in_seconds;

		$this->images_url = plugin_dir_url( VALET_CENTRAL_PLUGIN_FILE_PATH ) . 'images/';
	}

// UPDATE

	public function get_plugin_update_count() {
		return count( $this->get_plugin_update_data() );
	}

	public function get_plugin_update_data() {
		return $this->get_update_data( 'plugin' );
	}

	private function get_update_data( $object_type ) {
		if ( isset( self::$update_data_cache[$object_type] ) && is_array( self::$update_data_cache[$object_type] ) ) {
			return self::$update_data_cache[$object_type];
		}

		$update_res = $this->filter_res( $this->get_all_res(), 'update', $object_type );

		$class_instance = $this;
		$ret = array_map( function( $row ) use(  $class_instance, $object_type ) {
			$icon_url = '';
			if ( 'plugin' == $object_type ) {
				$icon_url = $class_instance->get_plugin_icon_url( $row['object_slug'] );
			} elseif ( 'wp' == $object_type ) {
				$icon_url = $class_instance->get_wp_icon_url();
			}

			$temp_ret = [
				'name'				=> $row['object_name'],
				'version'			=> empty( $row['object_subtype_from'] ) ? $row['object_subtype_to'] : $row['object_subtype_from'] . ' to ' . $row['object_subtype_to'],
				'datetime'			=> date( 'F d, Y, g:i a', strtotime( $row['create_date'] ) + $class_instance->gmt_offset_in_seconds ),
			];
			$temp_ret['icon_url'] = $icon_url;
			return $temp_ret;
		}, $update_res );

		unset( $update_res );

		self::$update_data_cache[$object_type] = $ret;
		return self::$update_data_cache[$object_type];
	}

	private function get_all_res() {
		if ( is_array( self::$all_res ) ) {
			return self::$all_res;
		}

		global $wpdb;

		$sql = 'SELECT * FROM ' . $wpdb->base_prefix . Valet_Central_Main::TBL_ACTIVITY .  $this->get_wh();
		self::$all_res = $wpdb->get_results( $sql, ARRAY_A );
		return self::$all_res;
	}

	private function get_wh() {
		if ( ! empty( self::$wh ) ) {
			return self::$wh;
		}

		global $wpdb;
		self::$wh = $wpdb->prepare( ' WHERE maintained_site_id = %d AND create_date >= %s AND create_date <= %s', $this->maintained_site_id, $this->from_date_in_gmt, $this->to_date_in_gmt );
		return self::$wh;
	}

	private function filter_res( $res, $action, $object_type ) {
		return array_filter( $res, function( $row ) use( $action, $object_type ) {
			$action_arr = is_array( $action ) ? $action : [$action];
			return in_array( $row['action'], $action_arr ) && $object_type === $row['object_type'];
		} );
	}

	private function get_plugin_icon_url( $plugin_slug ) {
		$icon_url = $this->images_url . 'plugin-icon.png';
		if ( ! empty( $plugin_slug ) ) {
			$temp = explode( '/', strtolower( $plugin_slug ) );
			$temp_url = 'https://ps.w.org/' . $temp[0] . '/assets/icon-128x128.png?rev='.time();
			
			if ( $this->is_icon_url_exists( $temp_url ) ) {
				$icon_url = $temp_url;
			}
		}
		return $icon_url;
	}

	private function is_icon_url_exists( $url ) {
		$response = wp_remote_head( $url );
		return ( ! is_wp_error( $response ) && $response['response']['code'] == 200 );
	}

	private function get_wp_icon_url( $plugin_slug ) {
		return $this->images_url . 'wp-core-icon.png';
	}

	public function get_theme_update_count() {
		return count( $this->get_theme_update_data() );
	}

	public function get_theme_update_data() {
		return $this->get_update_data( 'theme' );
	}

	public function get_wp_core_update_count() {
		return count( $this->get_wp_core_update_data() );
	}

	public function get_wp_core_update_data() {
		return $this->get_update_data( 'wp-core' );
	}


// INSTALL

	public function get_plugin_install_count() {
		return count( $this->get_plugin_install_data() );
	}

	public function get_plugin_install_data() {
		return $this->get_install_data( 'plugin' );
	}

	public function get_theme_install_count() {
		return count( $this->get_theme_install_data() );
	}

	public function get_theme_install_data() {
		return $this->get_install_data( 'theme' );
	}

	private function get_install_data( $object_type ) {
		if ( isset( self::$install_data_cache[$object_type] ) && is_array( self::$install_data_cache[$object_type] ) ) {
			return self::$install_data_cache[$object_type];
		}

		$install_res = $this->filter_res( $this->get_all_res(), 'install', $object_type );

		$class_instance = $this;
		$ret = array_map( function( $row ) use(  $class_instance, $object_type ) {
			return [
				'name'				=> $row['object_name'],
				'version'			=> empty( $row['object_subtype_from'] ) ? $row['object_subtype_to'] : $row['object_subtype_from'] . ' to ' . $row['object_subtype_to'],
				'datetime'			=> date( 'F d, Y, g:i a', strtotime( $row['create_date'] ) + $class_instance->gmt_offset_in_seconds ),
			];
		}, $install_res );

		unset( $install_res );

		self::$install_data_cache[$object_type] = $ret;
		return self::$install_data_cache[$object_type];
	}

// UNINSTALL

	public function get_plugin_uninstall_count() {
		return count( $this->get_plugin_uninstall_data() );
	}

	public function get_plugin_uninstall_data() {
		return $this->get_uninstall_data( 'plugin' );
	}

	public function get_theme_uninstall_count() {
		return count( $this->get_theme_uninstall_data() );
	}

	public function get_theme_uninstall_data() {
		return $this->get_uninstall_data( 'theme' );
	}

	private function get_uninstall_data( $object_type ) {
		if ( isset( self::$uninstall_data_cache[$object_type] ) && is_array( self::$uninstall_data_cache[$object_type] ) ) {
			return self::$uninstall_data_cache[$object_type];
		}

		$uninstall_res = $this->filter_res( $this->get_all_res(), ( 'theme' == $object_type ? 'delete' : 'uninstall' ), $object_type );

		$class_instance = $this;
		$ret = array_map( function( $row ) use(  $class_instance, $object_type ) {
			return [
				'name'				=> $row['object_name'],
				'version'			=> empty( $row['object_subtype_from'] ) ? $row['object_subtype_to'] : $row['object_subtype_from'] . ' to ' . $row['object_subtype_to'],
				'datetime'			=> date( 'F d, Y, g:i a', strtotime( $row['create_date'] ) + $class_instance->gmt_offset_in_seconds ),
			];
		}, $uninstall_res );

		unset( $uninstall_res );

		self::$uninstall_data_cache[$object_type] = $ret;
		return self::$uninstall_data_cache[$object_type];
	}

// ACTIVATE

	public function get_plugin_activate_count() {
		return count( $this->get_plugin_activate_data() );
	}

	public function get_plugin_activate_data() {
		return $this->get_activate_data( 'plugin' );
	}

	public function get_theme_activate_count() {
		return count( $this->get_theme_activate_data() );
	}

	public function get_theme_activate_data() {
		return $this->get_activate_data( 'theme' );
	}

	private function get_activate_data( $object_type ) {
		if ( isset( self::$activate_data_cache[$object_type] ) && is_array( self::$activate_data_cache[$object_type] ) ) {
			return self::$activate_data_cache[$object_type];
		}

		$activate_res = $this->filter_res( $this->get_all_res(), ['network-activate', 'activate', 'switch_theme'], $object_type );

		$class_instance = $this;
		$ret = array_map( function( $row ) use(  $class_instance, $object_type ) {
			$scope = '';
			if ( 'network-activate' == $row['action'] ) {
				$scope = 'Network Activated';
			} elseif ( '1' == $row['is_multisite'] && $row['site_id'] > 0  && ! empty( $row['site_name'] ) ) { // Activate
				$scope = 'Activated for Subsite: ' . esc_html( $row['site_name'] ) . ' (Subsite Id: '. $row['site_id'] . ')';
			}

			return [
				'name'				=> $row['object_name'] . ( empty( $scope ) ? '' : '<br /><small>' . $scope . '</small>' ),
				'version'			=> ( empty( $row['object_subtype_from'] ) ? $row['object_subtype_to'] : $row['object_subtype_from'] . ' to ' ) . $row['object_subtype_to'],
				'datetime'			=> date( 'F d, Y, g:i a', strtotime( $row['create_date'] ) + $class_instance->gmt_offset_in_seconds ),
			];
		}, $activate_res );

		unset( $activate_res );

		self::$activate_data_cache[$object_type] = $ret;
		return self::$activate_data_cache[$object_type];
	}

	public function is_multisite() {
		global $wpdb;

		$sql = 'SELECT COUNT( id ) FROM ' . $wpdb->base_prefix . Valet_Central_Main::TBL_ACTIVITY .  $this->get_wh() . ' AND is_multisite=1';
		return $wpdb->get_var( $sql );
	}

// DEACTIVATE

	public function get_plugin_deactivate_count() {
		return count( $this->get_plugin_deactivate_data() );
	}

	public function get_plugin_deactivate_data() {
		return $this->get_deactivate_data( 'plugin' );
	}

	public function get_theme_deactivate_count() {
		return count( $this->get_theme_deactivate_data() );
	}

	public function get_theme_deactivate_data() {
		return $this->get_deactivate_data( 'theme' );
	}

	private function get_deactivate_data( $object_type ) {
		if ( isset( self::$deactivate_data_cache[$object_type] ) && is_array( self::$deactivate_data_cache[$object_type] ) ) {
			return self::$deactivate_data_cache[$object_type];
		}

		$deactivate_res = $this->filter_res( $this->get_all_res(), ['network-deactivate', 'deactivate'], $object_type );

		$class_instance = $this;
		$ret = array_map( function( $row ) use(  $class_instance, $object_type ) {
			$scope = '';
			if ( 'network-deactivate' == $row['action'] ) {
				$scope = 'Network Deactivated';
			} elseif ( '1' == $row['is_multisite'] && $row['site_id'] > 0  && ! empty( $row['site_name'] ) ) { // Deactivate
				$scope = 'Deactivated for Subsite: ' . esc_html( $row['site_name'] ) . ' (Subsite Id: '. $row['site_id'] . ')';
			}
			return [
				'name'				=> $row['object_name'] . ( empty( $scope ) ? '' : '<br /><small>' . $scope . '</small>' ),
				'version'			=> empty( $row['object_subtype_from'] ) ? $row['object_subtype_to'] : $row['object_subtype_from'] . ' to ' . $row['object_subtype_to'],
				'datetime'			=> date( 'F d, Y, g:i a', strtotime( $row['create_date'] ) + $class_instance->gmt_offset_in_seconds ),
			];
		}, $deactivate_res );

		unset( $deactivate_res );

		self::$deactivate_data_cache[$object_type] = $ret;
		return self::$deactivate_data_cache[$object_type];
	}

// USER ADD AND DELETE

	public function get_user_action_count() {
		return count( $this->get_user_action_data() );
	}

	public function get_user_action_data() {
		if ( is_array( self::$user_action_data_cache ) ) {
			return self::$user_action_data_cache;
		}

		$user_res = array_filter( $this->get_all_res(), function( $row ) {
						return ( false !== stripos( $row['action'] , 'user' ) || in_array( $row['action'], ['create', 'delete'] ) )
									&&
								'user' === $row['object_type'];
					} );

		$user_action_data = [
			'create' => [
							'label' => 'Created',
							'is_subsite_display' => false,
			],
			'delete' => [
							'label' => 'Deleted',
							'is_subsite_display' => false,
			],
			'wpmu-new-user' => [
							'label' => 'Created Network User',
							'is_subsite_display' => false,
			],
			'wpmu-delete-user' => [
							'label' => 'Deleted Network User',
							'is_subsite_display' => false,
			],
			'granted-super-admin' => [
							'label' => 'Granted Super Administrator Privileges of MultiSite Network',
							'is_subsite_display' => false,
			],
			'revoked-super-admin' => [
							'label' => 'Revoked Super Administrator Privileges of MultiSite Network',
							'is_subsite_display' => false,
			],
			// subsite
			'add-user-to-blog' => [
							'label' => 'Added User',
							'is_subsite_display' => true,
			],
			'remove-user-from-blog' => [
							'label' => 'Deleted User',
							'is_subsite_display' => true,
			],
			'wpmu-signup-user' => [
							'label' => 'Invited User',
							'is_subsite_display' => true,
			],
			'wpmu-activate-user' => [
							'label' => 'Activate New User',
							'is_subsite_display' => true,
			],
		];
		$class_instance = $this;
		$ret = array_map( function( $row ) use( $class_instance, $user_action_data ) {
			$subsite_str = '';
			if ( true === $user_action_data[$row['action']]['is_subsite_display'] && '1' == $row['is_multisite'] && $row['site_id'] > 0  && ! empty( $row['site_name'] ) ) {
				$subsite_str = ' in Subsite: ' . esc_html( $row['site_name'] ) . ' (Subsite Id: '. $row['site_id'] . ')';
			}
			return [
				'name'				=> wp_strip_all_tags($row['object_name']) . ' (User Id: ' . $row['object_slug'] . ')',
				'action'			=> $user_action_data[$row['action']]['label'] . $subsite_str,
				'datetime'			=> date( 'F d, Y, g:i a', strtotime( $row['create_date'] ) + $class_instance->gmt_offset_in_seconds ),
			];
		}, $user_res );

		unset( $user_res );

		self::$user_action_data_cache = $ret;
		return self::$user_action_data_cache;
	}
}
