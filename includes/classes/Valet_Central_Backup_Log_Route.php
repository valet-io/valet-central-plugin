<?php
/**
 * Class Valet_Central_Backup_Log_Route
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

final class Valet_Central_Backup_Log_Route extends WP_REST_Controller {

	/**
	 * REST route namespace
	 *
	 * @var string
	 */
	protected $namespace;

	/**
	 * REST route base
	 *
	 * @var string
	 */
	private $base;

	/**
	 * Constructor of class
	 */
	public function __construct() {
		$this->namespace = Valet_Central_Main::NAMESPACE;
		$this->base      = '/backup-logs';
	}

	/**
	 * Get namespace base URI of rest notes route
	 *
	 * @return string
	 */
	public function get_namespace_base_url() {
		return $this->namespace . $this->base;
	}

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes() {
		$routes = array(
			array(
				'methods'  => WP_REST_Server::CREATABLE,
				'callback' => array( $this, 'create_item' ),
			),
			array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $this, 'get_items' ),
			),
		);

		foreach ( $routes as $route ) {
			$passed_args = array(
				'methods'             => $route['methods'],
				'callback'            => $route['callback'],
				'permission_callback' => [valet_central(), 'check_rest_permission'],
			);
			if ( isset( $args['args'] ) ) {
				$passed_args['args'] = $args['args'];
			}
			register_rest_route(
				$this->namespace,
				$this->base . ( empty( $route['base_postfix'] ) ? '' : $route['base_postfix'] ),
				$passed_args
			);

			unset( $passed_args );
		}

		unset( $routes );
		unset( $route );
	}

	/**
	 * Create a collection of item
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function create_item( $request ) {

		$maintained_site_table_name = $GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_MAINTAINED_SITES;
		$sql = $GLOBALS['wpdb']->prepare( 'SELECT * FROM ' . $maintained_site_table_name . ' WHERE user_id=%d AND uuid=%s', get_current_user_id(), $GLOBALS['wp_rest_application_password_uuid'] );
		$maintained_site_info = $GLOBALS['wpdb']->get_row( $sql );

		if ( is_null( $maintained_site_info ) ) {
			return new WP_Error( '401', "Maintained site record can't be found" );
		}

		$data	= [];
		$format = [];

		$data['maintained_site_id'] = $maintained_site_info->id;
		$format[] 					= '%d';

		$data['backup_date_time'] 	= sanitize_text_field( $request['backup_date_time'] );
		$format[]					= '%s';


		$data['create_date']		= current_time( 'mysql', true );
		$format[]					= '%s';

		$ret = $GLOBALS['wpdb']->insert(
			$this->get_table_name(),
			$data,
			$format
		);

		if ( $ret ) {
			return true;
		} else {
			return new WP_Error( 'cant-inset', "Can't insert the activity log." );
		}
	}

	/**
	 * Get a collection of items
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_items( $request ) {
		$site_domain = valet_central()->get_domain_from_url( sanitize_text_field( $request->get_header( 'referer' ) ) );
		if ( ! valet_central()->is_domain_match_application_password_uuid( $site_domain, $request ) ) {
			return new WP_Error( '401', "Application Password and request from domain don't match each other." );
		}

		$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

		$sql				  = $GLOBALS['wpdb']->prepare( 'SELECT * FROM ' . $this->get_table_name() . ' WHERE maintained_site_id=%d ORDER BY backup_date_time DESC;', intval( $maintained_site_info->id ) );
		$data				  = $GLOBALS['wpdb']->get_results( $sql, ARRAY_A );

		$response			  = rest_ensure_response( $data );

		$sql				  = $GLOBALS['wpdb']->prepare( 'SELECT count( id ) FROM ' . $this->get_table_name() . ' WHERE maintained_site_id=%d', intval( $maintained_site_info->id ) );
		$response->header( 'X-WP-Total', (int)  $GLOBALS['wpdb']->get_var( $sql ) );

		return $response;
	}

	/**
	 * Get Valets table name
	 *
	 * @return string valet backup log table name
	 */
	private function get_table_name() {
		return $GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_BACKUP;
	}
}