<?php
/**
 * Class Valet_Central_Report_Route
 */

use Carbon\Carbon;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
final class Valet_Central_Report_Route extends WP_REST_Controller {

	/**
	 * REST route namespace
	 *
	 * @var string
	 */
	protected $namespace;

	/**
	 * REST route base
	 *
	 * @var string
	 */
	private $base;

	private $activity_data_provider;
	private $speed_data_provider;
	private $backup_data_provider;

	/**
	 * Constructor of class
	 */
	public function __construct() {
		$this->namespace = Valet_Central_Main::NAMESPACE;
		$this->base      = '/report';
	}

	/**
	 * Get namespace base URI of rest notes route
	 *
	 * @return string
	 */
	public function get_namespace_base_url() {
		return $this->namespace . $this->base;
	}

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes() {
		register_rest_route(
			$this->namespace,
			$this->base,
			[
				'methods' 			  => 'POST',
				'callback' 			  => [$this, 'get_report'],
				'permission_callback' => [valet_central(), 'check_rest_permission'],
			]
		);
	}

	/**
	 * Get a collection of items
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_report( WP_REST_Request $request ) {
		require_once plugin_dir_path( VALET_CENTRAL_PLUGIN_FILE_PATH ) . '/vendor/autoload.php';
		require_once plugin_dir_path( VALET_CENTRAL_PLUGIN_FILE_PATH ) . '/libs/dompdf/autoload.inc.php';
		require_once plugin_dir_path( VALET_CENTRAL_PLUGIN_FILE_PATH ) . '/libs/GoogChart.class.php';

		global $wpdb;

		$site_domain = valet_central()->get_domain_from_url( sanitize_text_field( $request->get_header( 'referer' ) ) );
		if ( ! valet_central()->is_domain_match_application_password_uuid( $site_domain, $request ) ) {
			return new WP_Error( '401', "Application Password and request from domain don't match each other." );
		}

		$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

		$time_period 			 = sanitize_text_field( $request['time_period'] );
		$gmt_offset 	    	 = sanitize_text_field( $request['gmt_offset'] );
		$gmt_offset_in_seconds   = $gmt_offset * HOUR_IN_SECONDS;
		
		switch ( $time_period ) {
			case 'this_week':
				$start_of_this_week = Carbon::now()->startOfWeek();
				$end_of_this_week 	= Carbon::now()->endOfWeek();

				$start_of_this_week_timestamp = strtotime( $start_of_this_week );
				$end_of_this_week_timestamp   = strtotime( $end_of_this_week );

				$from_date_display = date( 'dS F Y', strtotime( "previous monday" ) );
				$to_date_display   = date( 'dS F Y', current_time( 'timestamp', true ) + $gmt_offset_in_seconds );

				$from_date_in_gmt = date( 'Y-m-d H:i:s',  $start_of_this_week_timestamp + $gmt_offset_in_seconds );
				$to_date_in_gmt   = date( 'Y-m-d H:i:s',  $end_of_this_week_timestamp + $gmt_offset_in_seconds );

				break;

			case 'last_week':
				$start_of_last_week = Carbon::now()->subDays( 7 )->startOfWeek();
				$end_of_last_week   = Carbon::now()->subDays( 7 )->endOfWeek();

				$start_of_last_week_timestamp = strtotime( $start_of_last_week );
				$end_of_last_week_timestamp   = strtotime( $end_of_last_week );

				$from_date_display = date( 'dS F Y', $start_of_last_week_timestamp );
				$to_date_display   = date( 'dS F Y', $end_of_last_week_timestamp );

				$from_date_in_gmt = date( 'Y-m-d H:i:s', $start_of_last_week_timestamp + $gmt_offset_in_seconds );
				$to_date_in_gmt   = date( 'Y-m-d H:i:s', $end_of_last_week_timestamp + $gmt_offset_in_seconds );

				break;

			case 'this_month':
				$start_of_this_month = Carbon::now()->startOfMonth();
				$end_of_this_month 	 = Carbon::now()->endOfMonth();

				$start_of_this_month_timestamp = strtotime( $start_of_this_month );
				$end_of_this_month_timestamp   = strtotime( $end_of_this_month );

				$from_date_display = date( 'dS F Y', $start_of_this_month_timestamp );
				$to_date_display   = date( 'dS F Y', current_time( 'timestamp', true ) + $gmt_offset_in_seconds );

				$from_date_in_gmt = date( 'Y-m-d H:i:s', $start_of_this_month_timestamp + $gmt_offset_in_seconds );
				$to_date_in_gmt   = date( 'Y-m-d H:i:s', $end_of_this_month_timestamp + $gmt_offset_in_seconds );

				break;

			case 'last_month':
				$start_of_last_month = Carbon::now()->startOfMonth()->subMonth();
				$end_of_last_month 	 = Carbon::now()->subMonth()->endOfMonth();

				$start_of_last_month_timestamp = strtotime( $start_of_last_month );
				$end_of_last_month_timestamp   = strtotime( $end_of_last_month );

				$from_date_display = date( 'dS F Y', $start_of_last_month_timestamp );
				$to_date_display   = date( 'dS F Y', $end_of_last_month_timestamp );

				$to_date_in_gmt   = date( 'Y-m-d H:i:s', $end_of_last_month_timestamp + $gmt_offset_in_seconds );
				
				break;
				
			case 'custom':
				$start_timestamp = strtotime( $request['from_date'] );
				$end_timestamp   = strtotime( $request['to_date'] . ' 23:59:59' );
				
				$from_date_display = date( 'dS F Y', $start_timestamp );
				$from_date_in_gmt = date( 'Y-m-d H:i:s', $start_of_last_month_timestamp + $gmt_offset_in_seconds );
				$to_date_display   = date( 'dS F Y', $end_timestamp );

				$from_date_in_gmt = date( 'Y-m-d H:i:s', $start_timestamp + $gmt_offset_in_seconds );
				$to_date_in_gmt   = date( 'Y-m-d H:i:s', $end_timestamp + $gmt_offset_in_seconds );

				break;
		}

		$maintained_site_id		= $maintained_site_info->id;

		$this->activity_data_provider	= new Valet_Central_Report_Activity_Data_Provider( $maintained_site_id, $from_date_in_gmt, $to_date_in_gmt, $gmt_offset_in_seconds );
		$this->speed_data_provider		= new Valet_Central_Report_Speed_Data_Provider( $maintained_site_id, $from_date_in_gmt, $to_date_in_gmt, $gmt_offset_in_seconds );
		$this->backup_data_provider		= new Valet_Central_Report_Backup_Data_Provider( $maintained_site_id, $from_date_in_gmt, $to_date_in_gmt, $gmt_offset_in_seconds );

		$upload_dir				= wp_upload_dir();

		$report_dir = valet_central()->get_pdf_report_dir_path() . $maintained_site_id;
        if ( ! file_exists( $report_dir ) ) {
			wp_mkdir_p( $report_dir );
		}

		$htaccess_file = $report_dir . '/.htaccess';
        if ( ! file_exists( $htaccess_file ) ) {
            $htaccess_file  = @fopen($htaccess_file, 'w');
            $content = <<<HTACCESS
Order Allow,Deny
Deny from All
HTACCESS;
            @fwrite( $htaccess_file, $content );
            @fclose( $htaccess_file );
        }

		$name_hash		= $this->generate_report_name( $maintained_site_info->name ) . "_" . $this->make_hash();
		$pdf_file_name  = $name_hash . '_report.pdf';
		
		$pdf_file_path = $report_dir . '/' . $pdf_file_name;
		$options = new Dompdf\Options();
		$options->set('defaultFont', 'Helvetica');
		$options->setFontHeightRatio(1.5);
		$options->set('isRemoteEnabled', true);
		$dompdf = new Dompdf\Dompdf($options);

		$dompdf->setPaper( array( 0, 0,  1080, 1920 ), 'landscape' );

		$page_right_left_margin = '6em';
		$html = '
		<html>
			<head>
			<style type="text/css">
				@page { margin: 50PX '. $page_right_left_margin . '; }
				header, footer { display: block; }
				header { top: -50px; margin-bottom: 5em; background-color: #1b2955; height: 3em; }
				footer { bottom: -50px; background-color: #f5f2f2; height: 5em; }
				header, footer { position: fixed; left: -' . $page_right_left_margin . '; right: -' . $page_right_left_margin . ';  }
				header p, footer p { margin-top: 7px; margin-left: 10px; font-size: 2em; }

				.page { margin-top: 8em; page-break-after: always; }
				.page:last-child { page-break-after: never; }

				h1 { font-size: 5em; }
				h2 {font-size: 3em; }
				.subsite-heading { color: #1b2955; font-size: 3em;}
				.large-title { font-size: 7.5em; color: #1b2955; text-transform: uppercase; }
				.report-by-title { font-size: 4em; position: relative; top: -1em; left: 3.5em; }
				.grey { color: #545050; }
				h2.grey { font-size: 5em; }
				h3 { font-size: 2.5em; }
				p.grey { font-size: 2.5em; }
				.date-range { font-size: 2em; }

				table.proper-font { font-size: 2.5em; }
				table.update thead { border-top: 1px solid #CCCCCC; border-bottom: 1px solid #CCCCCC; }
				table.update thead th {
					padding-top: 12px;
					padding-bottom: 12px;
					background-color: #1b2955;
					color: white;
				}
				table.update thead th, table.update tbody td { padding: 16px;}
				table.update tbody { padding: 12px; }
				table.update tbody tr.even { background-color: #f2f2f2; }
				table.update td { font-weight: 400; }
			</style>
		</head>
		<body>
		<header></header>
		<footer><p> ' . str_repeat( "&nbsp;", 6 ) . ' Website Report by VALET<p></footer>
		<main>
			'.
			$this->get_cover_page_html( $request, $maintained_site_info, $from_date_display, $to_date_display, $from_date_in_gmt, $to_date_in_gmt ) .
			$this->get_page2_html() .
			$this->get_overview_page_html( $request ) .
			$this->get_update_page_html() .
			$this->get_install_page_html() .
			$this->get_activate_page_html() .
			$this->get_deactivate_page_html() .
			$this->get_uninstall_page_html() .
			$this->get_user_page_html() .
			$this->get_speed_page_html() .
			$this->get_backup_page_html( $from_date_display, $to_date_display ) . 
			'
		</main>
		</body>
		</html>
		';
		$dompdf->loadHtml( $html );

		// Render the HTML as PDF
		$dompdf->render();

		$output = $dompdf->output();
		file_put_contents( $pdf_file_path, $output );

		$data = [
			'report_url' => $upload_dir['baseurl'] . '/valet-reports/' . $maintained_site_id . '/' . $pdf_file_name,
		];

		$response = rest_ensure_response( $data );

		return $response;
	}

	private function get_images_url() {
		return plugin_dir_url( VALET_CENTRAL_PLUGIN_FILE_PATH ) . 'images/';
	}

	
	private function get_page_title_html( $image_name, $title, $tagline  ) {
		return '<table style="margin: -3em 0 0 0; padding: 0">
					<tr>
						<td valign="middle">
							<img src="'.$this->get_images_url() . $image_name . '" alt="">
						</td>
						<td>
						</td>
						<td valign="top">
							<h1 class="page-title" style="margin: -0.6em 0 0 0; padding: 0;">'.$title.'</h1>
							<p class="grey" style="margin: -0.6em 0 0 0; padding: 0;">
								'.$tagline.'
							</p>
						</td>
					</tr>
				</table>
				<br /><br /><br />';
	}

	private function get_cover_page_html( $request, $maintained_site_info, $from_date_display, $to_date_display, $from_date_in_gmt, $to_date_in_gmt ) {
		$timezone_string    	 = sanitize_text_field( $request['timezone_string'] );

		$svg_logo				 = '<svg xmlns="http://www.w3.org/2000/svg" width="90" height="25" viewBox="0 0 90 25" id="svg-replaced-4" class="attachment-full size-full style-svg replaced-svg svg-replaced-4"><g><g><path fill="#171414" d="M18.104 4.468v10.705c0 .391-.16.776-.44 1.054l-6.467 6.447c-.566.565-1.554.565-2.12 0l-6.47-6.447a1.503 1.503 0 0 1-.437-1.054V4.468zM10.137 25c.907 0 1.76-.352 2.4-.99l6.469-6.448a3.348 3.348 0 0 0 .993-2.389V3.523a.946.946 0 0 0-.947-.944H1.222a.947.947 0 0 0-.948.944v11.65c0 .902.353 1.751.993 2.389l6.469 6.448c.64.638 1.494.99 2.4.99z"></path></g><g><path fill="#171414" d="M8.667 10.233l1.483 3.124 1.483-3.124h2.074l-2.966 5.83H9.56l-2.966-5.83z"></path></g><g><path fill="#171414" d="M29.506 10.633c-.528-1.33-.963-1.764-2.393-1.918V7.63h7.024v1.084c-1.492.092-2.021.402-2.021.928 0 .155.032.309.125.588l3.294 8.419h.217l2.798-7.15c.217-.588.372-1.083.372-1.455 0-.712-.466-1.144-1.802-1.361V7.63h5.563v1.053c-.964.155-1.772.835-2.393 2.32l-4.6 11.02h-1.616z"></path></g><g><path fill="#171414" d="M51.944 18.371c-.684.96-1.864 1.887-3.108 1.887-1.615 0-2.267-.99-2.267-2.197 0-1.95.962-2.6 5.375-3.559zm-8.422.402c0 1.733 1.213 3.25 3.824 3.25 1.833 0 3.356-1.053 4.599-2.446h.186c.187 1.703.56 2.384 2.207 2.384.933 0 1.647-.186 2.766-.588l-.124-.898c-2.052.248-2.3-.155-2.3-1.67V11.9c0-2.97-1.895-4.425-5.128-4.425-2.983 0-5.47 1.424-5.47 3.312 0 .897.623 1.702 1.648 1.702 1.057 0 1.647-.775 1.647-1.702 0-.433-.094-.929-.343-1.455.498-.464 1.089-.743 2.083-.743 2.02 0 2.828.96 2.828 3.9v.897c-.466.371-4.164.867-6.526 2.166-1.305.713-1.896 1.982-1.896 3.22z"></path></g><g><path fill="#171414" d="M58.068 20.878c1.802-.03 2.27-.34 2.27-1.083V3.235c0-.927-.592-1.485-2.395-1.516V.574L63.071.45v19.345c0 .743.435 1.053 2.268 1.083v1.145h-7.27z"></path></g><g><path fill="#171414" d="M69.133 13.294c.248-3.559 1.709-4.889 3.543-4.889 2.206 0 3.263 2.136 3.045 4.766zm-2.952 1.518c0 4.085 2.36 7.212 6.712 7.212 3.14 0 4.817-1.734 5.967-3.653l-.684-.65c-1.243 1.516-2.641 2.57-4.382 2.57-3.045 0-4.692-2.478-4.692-5.79v-.185l9.54.031c.249-3.467-1.523-7.026-5.75-7.026-4.04 0-6.711 3.25-6.711 7.49z"></path></g><g><path fill="#171414" d="M82.28 18.154V9.085H79.7v-.959l2.641-.557V3.576l2.672-1.114v5.17h4.445v1.391h-4.445v8.79c0 1.702.591 2.477 1.897 2.477.87 0 1.522-.65 2.206-1.672l.81.557c-1.152 2.074-2.209 2.847-4.198 2.847-2.206 0-3.449-1.36-3.449-3.868z"></path></g></g></svg>';
		
		return '<div class="page">
					<img src="data:image/svg+xml;base64,'.base64_encode($svg_logo).'" width="30%;" align="right"  alt="VALET.IO" />
					<br><br><br><br>
					<h1 class="grey large-title">'.esc_html($maintained_site_info->name).'</h1>
					<h1 class="report-by-title">Website Report by VALET</h1>
					<h2 class="grey">'.$maintained_site_info->url.'</h2>
					<div class="date-range">' . $from_date_display . ' - ' . $to_date_display . ' ' . ( empty( $timezone_string ) ? '' : '('. $timezone_string .' Time Zone)') .'</div>
					<!-- <div class="date-range">' . $from_date_in_gmt . ' - ' . $to_date_in_gmt . ' (GMT Time Zone)' . '</div> -->
				</div>';
	}

	private function get_page2_html() {
		return '<div class="page">
					'.$this->get_page_title_html( 'pdf.png', 'Our Report', 'Helping to strengthen your online presence' ).'
					<h2>Insights that help</h2>
					<p class="grey">
						The primary objective of this report is to present an overview of the health of your website. This report is generated based on the insights drawn through our systems and analysis by our team. These insights will help you keep track of all the changes as well as improvements on your website.
					</p>					
				</div>';
	}
	
	public function get_overview_page_html( $request ) {
		$installed_plugins_count 	   = sanitize_text_field($request['installed_plugins_count']);
		$installed_themes_count  	   = sanitize_text_field($request['installed_themes_count']);
		$wp_version				 	   = sanitize_text_field($request['wp_version']);
		$posts_count			 	   = sanitize_text_field($request['posts_count']);
		$comments_count			 	   = sanitize_text_field($request['comments_count']);
		$users_count			 	   = sanitize_text_field($request['users_count']);
		$administrator_role_user_count = sanitize_text_field($request['administrator_role_user_count']);

		$data = [
			'Plugins'			=> intval( $installed_plugins_count ),
			'Themes' 			=> intval( $installed_themes_count ),
			'WordPress Version' => esc_html( $wp_version ),
			'Posts'				=> intval( $posts_count ),
			'Comments'			=> intval( $comments_count ),
			'Users'				=> intval( $users_count ) . ' (' . $administrator_role_user_count . ' Administrator users out of it)',
		];
		$render_data = '';
		foreach ( $data as $key => $val ) {
			$render_data .= '<h2>' . $key . ': ' . $val . '</h2>';
		}
		return '<div class="page">
					'.$this->get_page_title_html( 'pdf.png', 'At a Glance', 'Giving you an overview of your website health' ).
					$render_data .
				'</div>';
	}

    private function get_default_name( $website_name ) {
        $special_chars = array( ".", "-" );
        $name          = date( 'Ymd' ) . '_' . sanitize_title( $website_name );
        $name          = substr(sanitize_file_name($name), 0, 40);
        $name          = str_replace($special_chars, '', $name);
        return $name;
    }

	private function generate_report_name( $website_name ) {
        $ticks = time();
        $ticks += ( (int) get_option('gmt_offset') * 3600 );

        $special_chars = array( ".", "-" );
        $name          = date( 'Y-m-d_H-i-s', $ticks ) . '_' . $this->get_default_name( $website_name );

        $name = str_replace($special_chars, '', $name);
        $sanitize_special_chars = array("?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}", "%", "+", chr(0));

        $name = preg_replace("#\x{00a0}#siu", ' ', $name);
        $name = str_replace($sanitize_special_chars, '', $name);
        $name = str_replace(array('%20', '+'), '-', $name);
        $name = preg_replace('/[\r\n\t -]+/', '-', $name);
        $name = trim($name, '.-_');

        $name = substr($name, 0, 40);

        return $name;
    }

    private function make_hash() {
        try {
            if (function_exists('random_bytes')) {
                return bin2hex(random_bytes(8)) . mt_rand(1000, 9999) . '_' . date("YmdHis");
            } else {
                return strtolower(md5(uniqid(rand(), true))) . '_' . date("YmdHis");
            }
        } catch (Exception $exc) {
            return strtolower(md5(uniqid(rand(), true))) . '_' . date("YmdHis");
        }
    }

	public function get_update_page_html() {
		$wp_core_update_count     = $this->activity_data_provider->get_wp_core_update_count();
		$plugin_update_count = $this->activity_data_provider->get_plugin_update_count();
		$theme_update_count = $this->activity_data_provider->get_theme_update_count();

		
		$html = '<div class="page page-update">
					' . $this->get_page_title_html( 'pdf.png', 'Updates', 'Always keep your Core, Plugins and Themes updated to their Latest Version' ) ;

		$meta_data = [
			[
				'wp-core-icon.png',
				'plugin-icon.png',
				'theme-icon.png',
			],
			[
				'Core Updates',
				'Plugin Updates',
				'Theme Updates',
			],
			[
				$wp_core_update_count,
				$plugin_update_count,
				$theme_update_count,
			]
		];

		$html .= $this->get_meta_table_html( $meta_data );

		if ( $wp_core_update_count > 0 ) {
			$headers = [
				'',
				'Version',
				'Updated on',
			];

			$render_data = $this->transform_update_raw_data_renderable( $this->activity_data_provider->get_wp_core_update_data() );

			$html .= '<br />';
			$html .= '<h2>Core Updates (' . $wp_core_update_count . ')</h2>';
			$html .= $this->get_table_html( $headers, $render_data );
		}

		if ( $plugin_update_count > 0 ) {
			$headers = [
				'Plugin',
				'Version',
				'Updated on',
			];

			$render_data = $this->transform_update_raw_data_renderable( $this->activity_data_provider->get_plugin_update_data() );
			
			$html .= '<br />';
			$html .= '<h2>Plugin Updates (' . $plugin_update_count . ')</h2>';
			$html .= $this->get_table_html( $headers, $render_data );
		}

		if ( $theme_update_count > 0 ) {
			$headers = [
				'Theme',
				'Version',
				'Updated on',
			];
			$render_data = $this->transform_update_raw_data_renderable( $this->activity_data_provider->get_theme_update_data() );
			
			$html .= '<br />';
			$html .= '<h2>Theme Updates (' . $theme_update_count . ')</h2>';
			$html .= $this->get_table_html( $headers, $render_data );
		}

		$html .= '</div>';
		return $html;
	}

	private function get_meta_table_html( $meta_data ) {
		$html =	'<table class="proper-font" style="padding: 40px; border: 1px solid #CCCCCC;" width=100%>
							<thead>;
								<tr>';
								foreach ( $meta_data[0] as $val ) {
									$html .= '<td align="center"><img src="' . $this->get_images_url() . $val . '" alt=""></td>';
								}
				  $html .= '</tr>
				  				<tr>';
								foreach ( $meta_data[1] as $val ) {
									$html .= '<th align="center">' . $val . '</th>';
								}
				   $html .= '</tr>
				   				<tr>';
								foreach ( $meta_data[2] as $val ) {
									$html .= '<td align="center">' . $val . '</td>';
								}
		$html .=			'</tr>
							</thead>
						</table>';
		$html .= '<br /><br />';
		return $html;
	}

	private function transform_update_raw_data_renderable( $data ) {
		return array_map( function( $val ) {
			return [
				$val['name'],
				$val['version'],
				$val['datetime']
			];
		}, $data );
	}

	private function get_table_html( $headers, $data ) {
		$aligns = [ 'left', 'center', 'right'];
		$html = '<table class="update proper-font" width=100%>
					<thead>
					<tr>';
		foreach ( $headers as $key => $header ) {
			$html .=	'<th valign= "middle" align ="' . $aligns[$key] . '" width="33.333%">' . $header . '</th>';
		}
		$html .=	'</tr>
					</thead>
					<tbody>';
		
		$i = 0;
		foreach ( $data as $data_val ) {
			$html .= '
					 <tr ' . ( 0 == $i % 2 ? 'class="even"' : '' )  . '>';
			foreach ( $data_val as $key => $val ) {
					$html .= '<td valign= "middle" align ="' . $aligns[$key] . '" width="33.333%">' . $val . '</td>';
			}
			$html .= '</tr>';
			$i ++;
		}

		$html .= '</tbody>
				  </table>';

		return $html;
	}

	public function get_install_page_html() {
		$plugin_install_count = $this->activity_data_provider->get_plugin_install_count();
		$theme_install_count = $this->activity_data_provider->get_theme_install_count();

		
		$html = '<div class="page page-update">
					' . $this->get_page_title_html( 'pdf.png', 'Installs', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit' ) ;

		$meta_data = [
			[
				'plugin-icon.png',
				'theme-icon.png',
			],
			[
				'Plugin Installs',
				'Theme Installs',
			],
			[
				$plugin_install_count,
				$theme_install_count,
			]
		];

		$html .= $this->get_meta_table_html( $meta_data );

		if ( $plugin_install_count > 0 ) {
			$headers = [
				'Plugin',
				'Version',
				'Installed on',
			];

			$render_data = $this->transform_update_raw_data_renderable( $this->activity_data_provider->get_plugin_install_data() );
			
			$html .= '<br />';
			$html .= '<h2>Plugin Installs (' . $plugin_install_count . ')</h2>';
			$html .= $this->get_table_html( $headers, $render_data );
		}

		if ( $theme_install_count > 0 ) {
			$headers = [
				'Theme',
				'Version',
				'Installed on',
			];
			$render_data = $this->transform_update_raw_data_renderable( $this->activity_data_provider->get_theme_install_data() );
			
			$html .= '<br />';
			$html .= '<h2>Theme Installs (' . $theme_install_count . ')</h2>';
			$html .= $this->get_table_html( $headers, $render_data );
		}

		$html .= '</div>';
		return $html;
	}

	public function get_activate_page_html() {
		$is_multisite = $this->activity_data_provider->is_multisite();
		$plugin_activate_count = $this->activity_data_provider->get_plugin_activate_count();
		$theme_activate_count = $this->activity_data_provider->get_theme_activate_count();
		
		$html = '<div class="page page-update">
					' . $this->get_page_title_html( 'pdf.png', 'Activates', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit' ) ;

		$meta_data = [
			[
				'plugin-icon.png',
				'theme-icon.png',
			],
			[
				'Plugin Activates',
				'Theme Activates',
			],
			[
				$plugin_activate_count,
				$theme_activate_count,
			]
		];

		$html .= $this->get_meta_table_html( $meta_data );

		if ( $plugin_activate_count > 0 ) {
			$headers = [
				'Plugin' . ( $is_multisite ? ' & Scope' : '' ),
				'Version',
				'Activated on',
			];

			$render_data = $this->transform_update_raw_data_renderable( $this->activity_data_provider->get_plugin_activate_data() );
			
			$html .= '<br />';
			$html .= '<h2>Plugin Activates (' . $plugin_activate_count . ')</h2>';
			$html .= $this->get_table_html( $headers, $render_data );
		}

		if ( $theme_activate_count > 0 ) {
			$headers = [
				'Theme' . ( $is_multisite ? ' & Scope' : '' ),
				'Version',
				'Activated on',
			];
			$render_data = $this->transform_update_raw_data_renderable( $this->activity_data_provider->get_theme_activate_data() );
			
			$html .= '<br />';
			$html .= '<h2>Theme Activates (' . $theme_activate_count . ')</h2>';
			$html .= $this->get_table_html( $headers, $render_data );
		}

		$html .= '</div>';
		return $html;
	}

	public function get_deactivate_page_html() {
		$is_multisite = $this->activity_data_provider->is_multisite();
		$plugin_deactivate_count = $this->activity_data_provider->get_plugin_deactivate_count();
		$theme_deactivate_count = $this->activity_data_provider->get_theme_deactivate_count();
		
		$html = '<div class="page page-update">
					' . $this->get_page_title_html( 'pdf.png', 'Deactivates', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit' ) ;

		$meta_data = [
			[
				'plugin-icon.png',
				'theme-icon.png',
			],
			[
				'Plugin Deactivates',
				'Theme Deactivates',
			],
			[
				$plugin_deactivate_count,
				$theme_deactivate_count,
			]
		];

		$html .= $this->get_meta_table_html( $meta_data );

		if ( $plugin_deactivate_count > 0 ) {
			$headers = [
				'Plugin' . ( $is_multisite ? ' & Scope' : '' ),
				'Version',
				'Deactivated on',
			];

			$render_data = $this->transform_update_raw_data_renderable( $this->activity_data_provider->get_plugin_deactivate_data() );
			
			$html .= '<br />';
			$html .= '<h2>Plugin Deactivates (' . $plugin_deactivate_count . ')</h2>';
			$html .= $this->get_table_html( $headers, $render_data );
		}

		if ( $theme_deactivate_count > 0 ) {
			$headers = [
				'Theme' . ( $is_multisite ? ' & Scope' : '' ),
				'Version',
				'Deactivated on',
			];
			$render_data = $this->transform_update_raw_data_renderable( $this->activity_data_provider->get_theme_deactivate_data() );
			
			$html .= '<br />';
			$html .= '<h2>Theme Deactivates (' . $theme_deactivate_count . ')</h2>';
			$html .= $this->get_table_html( $headers, $render_data );
		}

		$html .= '</div>';
		return $html;
	}

	public function get_uninstall_page_html() {
		$plugin_uninstall_count = $this->activity_data_provider->get_plugin_uninstall_count();
		$theme_uninstall_count = $this->activity_data_provider->get_theme_uninstall_count();
		
		$html = '<div class="page page-update">
					' . $this->get_page_title_html( 'pdf.png', 'Uninstalls', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit' ) ;

		$meta_data = [
			[
				'plugin-icon.png',
				'theme-icon.png',
			],
			[
				'Plugin Uninstalls',
				'Theme Uninstalls',
			],
			[
				$plugin_uninstall_count,
				$theme_uninstall_count,
			]
		];

		$html .= $this->get_meta_table_html( $meta_data );

		if ( $plugin_uninstall_count > 0 ) {
			$headers = [
				'Plugin',
				'Version',
				'Uninstalled on',
			];

			$render_data = $this->transform_update_raw_data_renderable( $this->activity_data_provider->get_plugin_uninstall_data() );
			
			$html .= '<br />';
			$html .= '<h2>Plugin Uninstalls (' . $plugin_uninstall_count . ')</h2>';
			$html .= $this->get_table_html( $headers, $render_data );
		}

		if ( $theme_uninstall_count > 0 ) {
			$headers = [
				'Theme',
				'Version',
				'Uninstalled on',
			];
			$render_data = $this->transform_update_raw_data_renderable( $this->activity_data_provider->get_theme_uninstall_data() );
			
			$html .= '<br />';
			$html .= '<h2>Theme Uninstalls (' . $theme_uninstall_count . ')</h2>';
			$html .= $this->get_table_html( $headers, $render_data );
		}

		$html .= '</div>';
		return $html;
	}

	public function get_user_page_html() {

		$html = '<div class="page page-update">
					' . $this->get_page_title_html( 'pdf.png', 'Users', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit' ) ;

		$user_action_count = $this->activity_data_provider->get_user_action_count();

		if ( $user_action_count > 0 ) {
			$headers = [
				'User',
				'Action',
				'Action on',
			];

			$render_data = array_map( function( $val ) {
				return [
					$val['name'],
					$val['action'],
					$val['datetime']
				];
			}, $this->activity_data_provider->get_user_action_data() );
			
			$html .= '<br />';
			$html .= '<h2>User Related Actions</h2>';
			$html .= $this->get_table_html( $headers, $render_data );
		} else {
			$html .= '<p class="grey">There are no user-related actions during this time interval.</p>';
		}

		$html .= '</div>';
		return $html;
	}

	public function get_speed_page_html() {
		
		$html = '<div class="page page-speed">
					' . $this->get_page_title_html( 'pdf.png', 'PERFORMANCE', 'The faster you load, the happier your visitors are' ) . '
					<br /><br /><br /><br />';
		$is_multisite = $this->speed_data_provider->is_multisite();

		$last_data 	  = $this->speed_data_provider->get_last_data();
		// $chart_data 	  = $this->speed_data_provider->get_data();

		foreach ( $last_data as $subsite_last_data ) {
			if ( $is_multisite ) {
				$html .= '<h2 class="subsite-heading">Subsite: ' . $subsite_last_data['site_name'] . '</h2>';
			}

			if ( empty( $subsite_last_data['info'] ) ) {
				$html .= '<p class="grey">No recent performance data was found.</p>';
			} else {
				array_walk( $subsite_last_data['info'], function( $speed_info ) use( &$html ) {
					$html .= '<h3>' . $speed_info->title . ': ' . $speed_info->displayValue . '</h3>';
					$html .= '<p class="grey">' . $speed_info->description . '</p>';
				});
			}
			// $chart_data[$subsite_last_data['site_id']]
		}

		$html .= '</div>';
		return $html;
	}

	public function get_backup_page_html( $from_date_in_display, $to_date_in_display ) {
		
		$html = '<div class="page page-backup">
					' . $this->get_page_title_html( 'pdf.png', 'BACKUPS', 'The faster you load, the happier your visitors are' ) . '
					<br /><br /><br /><br />';

		$html .= '<h2>What is included in the backup?</h2>
				  <p class="grey">Intelligent backups that sync only incremental changes for minimal server load. Your website will never have a slow day. Our incremental cloud based (offsite) technology backs up not only all the files and content of the website but also the database as well.</p>
				  <h2>Importance of Backup?</h2>
				  <p class="grey">Backups are your get-out-of-jail ticket when things go south. You can use a backup to restore your website and get it back online in no time.</p>';


		$backup_data 	  = $this->backup_data_provider->get_data();
		$date_range_period = new DatePeriod(
			new DateTime( $from_date_in_display ),
			new DateInterval( 'P1D' ),
			new DateTime( date( 'dS F Y', strtotime( $to_date_in_display ) + DAY_IN_SECONDS ) )
	    );
		$render_data = [];
		foreach ( $date_range_period as $key => $value ) {
			$raw_date = $value->format('j M');
			if ( isset( $backup_data[$raw_date] ) && $backup_data[$raw_date] > 0 ) {
				$temp_image_name = 'right-mark.png';
				$temp_alt = 'Yes';
			} else {
				$temp_image_name = 'cross-mark.png';
				$temp_alt = 'No';
			}
			$is_backup_taken = '<img src="' . $this->get_images_url() . $temp_image_name . '" alt="' . $temp_alt . '" style="width: 32px; height: 32px;"/>';
			$render_data[] = [
				$raw_date,
				$is_backup_taken,
				isset( $backup_data[$raw_date] ) ? $backup_data[$raw_date] : 0,
			];
		}
		if ( ! empty( $render_data ) ) {
			$html .= $this->get_table_html(
				[
					'Date',
					'Backup Taken',
					'No. of Backups',
				],
				$render_data
			);
		}
				
		$html .= '</div>';

		return $html;
	}
}
