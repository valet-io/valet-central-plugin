<?php
/**
 * Class Valet_Central_Activity_Log_Route
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

final class Valet_Central_Activity_Log_Route extends WP_REST_Controller {

	/**
	 * REST route namespace
	 *
	 * @var string
	 */
	protected $namespace;

	/**
	 * REST route base
	 *
	 * @var string
	 */
	private $base;

	/**
	 * Constructor of class
	 */
	public function __construct() {
		$this->namespace = Valet_Central_Main::NAMESPACE;
		$this->base      = '/activity-logs';
	}

	/**
	 * Get namespace base URI of rest notes route
	 *
	 * @return string
	 */
	public function get_namespace_base_url() {
		return $this->namespace . $this->base;
	}

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes() {
		$routes = array(
			array(
				'methods'  => WP_REST_Server::CREATABLE,
				'callback' => array( $this, 'create_item' ),
			),
			array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $this, 'get_items' ),
			),
		);

		foreach ( $routes as $route ) {
			$passed_args = array(
				'methods'             => $route['methods'],
				'callback'            => $route['callback'],
				'permission_callback' => [valet_central(), 'check_rest_permission'],
			);
			if ( isset( $args['args'] ) ) {
				$passed_args['args'] = $args['args'];
			}
			register_rest_route(
				$this->namespace,
				$this->base . ( empty( $route['base_postfix'] ) ? '' : $route['base_postfix'] ),
				$passed_args
			);

			unset( $passed_args );
		}

		unset( $routes );
		unset( $route );
	}

	/**
	 * Create a collection of item
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function create_item( $request ) {

		$maintained_site_table_name = $GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_MAINTAINED_SITES;
		$sql = $GLOBALS['wpdb']->prepare( 'SELECT * FROM ' . $maintained_site_table_name . ' WHERE user_id=%d AND uuid=%s', get_current_user_id(), $GLOBALS['wp_rest_application_password_uuid'] );
		$maintained_site_info = $GLOBALS['wpdb']->get_row( $sql );

		if ( is_null( $maintained_site_info ) ) {
			return new WP_Error( '401', "Maintained site record can't be found" );
		}

		$data	= [];
		$format = [];

		$data['maintained_site_id'] = $maintained_site_info->id;
		$format[] = '%d';

		$data['action'] = $request['action'];
		$format[] = '%s';

		$data['object_type'] = $request['object_type'];
		$format[] = '%s';

		$data['object_slug'] = $request['object_slug'];
		$format[] = '%s';

		$data['object_name'] = $request['object_name'];
		$format[] = '%s';

		if ( isset ( $request['object_subtype_from'] ) ) {
			$data['object_subtype_from'] = $request['object_subtype_from'];
			$format[] = '%s';
		}

		if ( isset ( $request['object_subtype_to'] ) ) {
			$data['object_subtype_to'] = $request['object_subtype_to'];
			$format[] = '%s';
		}

		if ( isset( $request['user_id_of_action_by'] ) && intval( $request['user_id_of_action_by'] ) > 0 ) {
			$data['user_id_of_action_by'] = intval( $request['user_id_of_action_by'] );
			$format[] = '%d';
		}

		if ( isset( $request['display_name_of_action_by'] ) ) {
			$data['display_name_of_action_by'] = $request['display_name_of_action_by'];
			$format[] = '%s';
		}

		if ( isset ( $request['email_address_of_action_by'] ) ) {
			$data['email_address_of_action_by'] = $request['email_address_of_action_by'];
			$format[] = '%s';
		}

		if ( isset ( $request['is_multisite'] ) && in_array( $request['is_multisite'], [1, 0] ) ) {
			$data['is_multisite'] = $request['is_multisite'];
			$format[] = '%d';
		}

		if ( isset( $request['site_id'] ) && intval( $request['site_id'] ) > 0 ) {
			$data['site_id'] = intval( $request['site_id'] );
			$format[] = '%d';
		}

		if ( isset ( $request['site_name'] ) ) {
			$data['site_name'] = $request['site_name'];
			$format[] = '%s';
		}

		$data['ip'] = $request['ip'];
		$format[] = '%s';

		$data['create_date'] = current_time( 'mysql', true );
		$format[] = '%s';

		$ret = $GLOBALS['wpdb']->insert(
			$this->get_table_name(),
			$data,
			$format
		);

		if ( $ret ) {
			return true;
		} else {
			return new WP_Error( 'cant-inset', "Can't insert the activity log." );
		}
	}

	/**
	 * Get a collection of items
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_items( $request ) {
		$site_domain = valet_central()->get_domain_from_url( sanitize_text_field( $request->get_header( 'referer' ) ) );
		if ( ! valet_central()->is_domain_match_application_password_uuid( $site_domain, $request ) ) {
			return new WP_Error( '401', "Application Password and request from domain don't match each other." );
		}

		$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

		$wh_ext = '';
		if ( ! empty( $request['search_object_type'] ) ) {
			$wh_ext = $GLOBALS['wpdb']->prepare( ' AND object_type=%s ', sanitize_text_field( $request['search_object_type'] ) );
		}
		if ( ! empty( $request['search'] ) ) {
			$wh_ext = $GLOBALS['wpdb']->prepare( ' AND ( object_slug LIKE %s OR object_name LIKE %s )', '%' . $GLOBALS['wpdb']->esc_like( sanitize_text_field( $request['search'] ) ) . '%', '%' . $GLOBALS['wpdb']->esc_like( sanitize_text_field( $request['search'] ) ) . '%' );
		}

		$sql = $GLOBALS['wpdb']->prepare( 'SELECT * FROM ' . $this->get_table_name() . ' WHERE maintained_site_id=%d ' . $wh_ext . ' ORDER BY id DESC;', intval( $maintained_site_info->id ) );
		$data = $GLOBALS['wpdb']->get_results( $sql, ARRAY_A );

		$response = rest_ensure_response( $data );

		$sql = $GLOBALS['wpdb']->prepare( 'SELECT count( id ) FROM ' . $this->get_table_name() . ' WHERE maintained_site_id=%d', intval( $maintained_site_info->id ) );
		$response->header( 'X-WP-Total', (int)  $GLOBALS['wpdb']->get_var( $sql ) );

		return $response;
	}

	/**
	 * Get Valets table name
	 *
	 * @return string valet notes table name
	 */
	private function get_table_name() {
		return $GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_ACTIVITY;
	}
}
