<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

final class Valet_Central_Report_Speed_Data_Provider {

	private $maintained_site_id;
	private $from_date_in_gmt;
	private $to_date_in_gmt;
	private $gmt_offset_in_seconds;

	private $images_url;

	private static $is_multisite = null;
	private static $wh = null;
	private static $unique_subsite_ids = null;
	private static $subsite_names = null;

	public function __construct( $maintained_site_id, $from_date_in_gmt, $to_date_in_gmt, $gmt_offset_in_seconds ) {
		$this->maintained_site_id    = intval( $maintained_site_id );
		$this->from_date_in_gmt      = $from_date_in_gmt;
		$this->to_date_in_gmt        = $to_date_in_gmt;
		$this->gmt_offset_in_seconds = $gmt_offset_in_seconds;
	}

	public function get_data() {
		$ret = [];

		if ( $this->is_multisite() ) {
			$subsite_ids = $this->get_unique_subsite_ids();
			foreach ( $subsite_ids as $sub_site_id ) {
				$ret[$sub_site_id] = $this->get_data_for_subsite( $sub_site_id );
			}
		} else {
			$ret[0] = $this->get_data_for_subsite( 0 );
		}

		return $ret;
	}

	public function is_multisite() {
		if ( is_bool( self::$is_multisite ) ) {
			self::$is_multisite;
		}

		$sql = 'SELECT distinct( is_multisite ) FROM ' . $this->get_table_name() . $this->get_wh();
		$multisite_vals = $GLOBALS['wpdb']->get_col( $sql );
		self::$is_multisite = in_array( '1', $multisite_vals );
		
		return self::$is_multisite;
	}

	private function get_table_name() {
		return $GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_SPEED;
	}

	private function get_wh() {
		if ( ! empty( self::$wh ) ) {
			return self::$wh;
		}

		self::$wh = $GLOBALS['wpdb']->prepare( ' WHERE maintained_site_id = %d AND create_date >= %s AND create_date <= %s', $this->maintained_site_id, $this->from_date_in_gmt, $this->to_date_in_gmt );
		return self::$wh;
	}

	private function get_unique_subsite_ids() {
		if ( is_array( self::$unique_subsite_ids ) ) {
			return self::$unique_subsite_ids;
		}
		$sql = 'SELECT distinct( site_id ) FROM ' . $this->get_table_name() . $this->get_wh() . ' ORDER BY site_id ASC';
	
		self::$unique_subsite_ids = $GLOBALS['wpdb']->get_col( $sql );
		return self::$unique_subsite_ids;
	}

	private function get_data_for_subsite( $subsite_id = 0 ) {
		$sql = $GLOBALS['wpdb']->prepare( 'SELECT speed_index, create_date, site_id, site_name FROM ' . $this->get_table_name() . $this->get_wh() . ' AND site_id = %d ORDER BY create_date ASC', $subsite_id );
		$res = $GLOBALS['wpdb']->get_results( $sql, ARRAY_A );

		$ret = [];
		foreach ( $res as $row ) {
			$speed_index = json_decode( $row['speed_index'] );

			if ( ! isset( $speed_index->numericValue ) ) {
				continue;
			}
			$create_timestamp = strtotime( $row['create_date'] ) + $this->gmt_offset_in_seconds;
			$create_date = date( 'j M', $create_timestamp );
			$ret[$create_date] = number_format( $speed_index->numericValue / 1000, 2 );
		}

		return $ret;
	}

	public function get_subsite_name( $subsite_id ) {
		if ( isset( self::$subsite_names[$subsite_id] ) ) {
			return self::$subsite_names[$subsite_id];
		}
		$sql = $GLOBALS['wpdb']->prepare( 'SELECT site_name FROM ' . 																							$this->get_table_name() . $this->get_wh() . ' AND 													site_id = %d ORDER BY id DESC LIMIT 1', $subsite_id );
		self::$subsite_names[$subsite_id] = $GLOBALS['wpdb']->get_var( $sql );
		return self::$subsite_names[$subsite_id];
	}

	public function get_last_data() {
		$ret = [];

		if ( $this->is_multisite() ) {
			$subsite_ids = $this->get_unique_subsite_ids();
			foreach ( $subsite_ids as $sub_site_id ) {
				$ret[$sub_site_id] = $this->get_last_data_for_subsite( $sub_site_id );
			}
		} else {
			$ret[0] = $this->get_last_data_for_subsite( 0 );
		}
		return $ret;
	}

	public function get_last_data_for_subsite( $subsite_id = 0 ) {
		$sql = $GLOBALS['wpdb']->prepare( 'SELECT * FROM ' . $this->get_table_name() . $this->get_wh() . ' AND 				site_id = %d ORDER BY create_date DESC LIMIT 1', $subsite_id );
		$row = $GLOBALS['wpdb']->get_row( $sql, ARRAY_A );

		$other_speed_data =  json_decode( $row['other_speed_data'] );
		$audits = $other_speed_data->audits;
		$data_keys = [
			'first-contentful-paint',
			'speed-index',
			'largest-contentful-paint',
			'interactive',
		];

		$info = [];
		foreach ( $data_keys as $data_key ) {
			if ( ! property_exists( $audits, $data_key ) ) {
				continue;
			}
			$info[$data_key] = $audits->$data_key;
		}

		$ret = [
			'info' => $info,
			'site_id' => $row['site_id'],
			'site_name' => $row['site_name'],
			'create_date' => $row['create_date'],
		];
	
		return $ret;
	}
}
