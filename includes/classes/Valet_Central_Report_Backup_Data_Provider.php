<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

final class Valet_Central_Report_Backup_Data_Provider {

	private $maintained_site_id;
	private $from_date_in_gmt;
	private $to_date_in_gmt;
	private $gmt_offset_in_seconds;

	public function __construct( $maintained_site_id, $from_date_in_gmt, $to_date_in_gmt, $gmt_offset_in_seconds ) {
		$this->maintained_site_id    = intval( $maintained_site_id );
		$this->from_date_in_gmt      = $from_date_in_gmt;
		$this->to_date_in_gmt        = $to_date_in_gmt;
		$this->gmt_offset_in_seconds = $gmt_offset_in_seconds;
	}

	public function get_data() {
		global $wpdb;
		
		$sql = $wpdb->prepare( 'SELECT backup_date_time FROM ' . $wpdb->base_prefix . Valet_Central_Main::TBL_BACKUP .' WHERE maintained_site_id=%d AND create_date >= %s AND create_date <= %s ORDER BY create_date ASC', $this->maintained_site_id, $this->from_date_in_gmt, $this->to_date_in_gmt );
		$backup_date_times_in_gmt = $wpdb->get_col( $sql );

		$temp = [];
		foreach ( $backup_date_times_in_gmt as $backup_date_time_in_gmt ) {
			$backup_timestamp = strtotime( $backup_date_time_in_gmt ) + $this->gmt_offset_in_seconds;
			$temp[date( 'j M', $backup_timestamp )][] = $backup_timestamp; 
		}

		$ret = [];
		foreach ( $temp as $backup_date_time_display => $val ) {
			$ret[$backup_date_time_display] = count( $val );
		}

		return $ret;
	}
}
