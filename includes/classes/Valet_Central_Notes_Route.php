<?php
/**
 * Class Valet_Central_Notes_Route
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

final class Valet_Central_Notes_Route extends WP_REST_Controller {

	/**
	 * REST route namespace
	 *
	 * @var string
	 */
	protected $namespace;

	/**
	 * REST route base
	 *
	 * @var string
	 */
	private $base;

	/**
	 * Constructor of class
	 */
	public function __construct() {
		$this->namespace = Valet_Central_Main::NAMESPACE;
		$this->base      = '/notes';
	}

	/**
	 * Get namespace base URI of rest notes route
	 *
	 * @return string
	 */
	public function get_namespace_base_url() {
		return $this->namespace . $this->base;
	}

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes() {
		$routes = array(
			array(
				'methods'  => WP_REST_Server::CREATABLE,
				'callback' => array( $this, 'create_item' ),
			),
			array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $this, 'get_items' ),
			),
			array(
				'base_postfix' => '/(?P<id>[\d]+)',
				'methods'      => WP_REST_Server::READABLE,
				'callback'     => array( $this, 'get_item' ),
			),
			array(
				'base_postfix' => '/(?P<id>[\d]+)',
				'methods'      => WP_REST_Server::DELETABLE,
				'callback'     => array( $this, 'delete_item' ),
				'args'         => array(
					'id' => array(
						'validate_callback' => function ( $param, $request, $key ) {
							return is_numeric( $param );
						},
					),
				),
			),
			array(
				'base_postfix' => '/(?P<id>[\d]+)',
				'methods'      => WP_REST_Server::EDITABLE,
				'callback'     => array( $this, 'update_item' ),
				'args'         => array(
					'id' => array(
						'validate_callback' => function ( $param, $request, $key ) {
							return is_numeric( $param );
						},
					),
				),
			),
		);

		foreach ( $routes as $route ) {
			$passed_args = array(
				'methods'             => $route['methods'],
				'callback'            => $route['callback'],
				'permission_callback' => [valet_central(), 'check_rest_permission'],
			);
			if ( isset( $args['args'] ) ) {
				$passed_args['args'] = $args['args'];
			}
			register_rest_route(
				$this->namespace,
				$this->base . ( empty( $route['base_postfix'] ) ? '' : $route['base_postfix'] ),
				$passed_args
			);

			unset( $passed_args );
		}

		unset( $routes );
		unset( $route );
	}

	/**
	 * Get a collection of items
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_items( $request ) {
		$site_domain = valet_central()->get_domain_from_url( sanitize_text_field( $request->get_header( 'referer' ) ) );
		if ( ! valet_central()->is_domain_match_application_password_uuid( $site_domain, $request ) ) {
			return new WP_Error( '401', "Application Password and request from domain don't match each other." );
		}
		$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

		$search = sanitize_text_field( $request['search'] );
		$wh_ext = '';
		if ( ! empty( $search ) ) {
			$wh_ext = $GLOBALS['wpdb']->prepare( ' AND note LIKE %s ', '%' . $GLOBALS['wpdb']->esc_like( $search ) . '%' );
		}

		$sql = $GLOBALS['wpdb']->prepare( 'SELECT SQL_CALC_FOUND_ROWS id, note, create_date FROM ' . $this->get_note_table_name() . ' WHERE maintained_site_id=%d ' . $wh_ext . ' ORDER BY id DESC;', intval( $maintained_site_info->id ) );
		$data = $GLOBALS['wpdb']->get_results( $sql , ARRAY_A );

		$response = rest_ensure_response( $data );
		
		$sql = $GLOBALS['wpdb']->prepare( 'SELECT count( id ) FROM ' . $this->get_note_table_name() . ' WHERE maintained_site_id=%d', intval( $maintained_site_info->id ) );
		$response->header( 'X-WP-Total', (int)  $GLOBALS['wpdb']->get_var( $sql ) );

		return $response;
	}

	/**
	 * Get a collection of items
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_item( $request ) {
		$site_domain = valet_central()->get_domain_from_url( sanitize_text_field( $request->get_header( 'referer' ) ) );
		if ( ! valet_central()->is_domain_match_application_password_uuid( $site_domain, $request ) ) {
			return new WP_Error( '401', "Application Password and request from domain don't match each other." );
		}
		$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

		$sql  = $GLOBALS['wpdb']->prepare( 'SELECT id, note, create_date FROM ' . $this->get_note_table_name() . ' WHERE id=%d AND maintained_site_id=%d;', intval( $request['id'] ), intval( $maintained_site_info->id ) );
		$data = $GLOBALS['wpdb']->get_row( $sql );

		if ( is_object( $data ) ) {
			return $data;
		}

		return new WP_Error( 'cant-find-note', __( 'The given ID doesn\'t found!', 'valet-suport' ) );
	}

	/**
	 * Create one item from the collection
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function create_item( $request ) {
		$site_domain = valet_central()->get_domain_from_url( sanitize_text_field( $request->get_header( 'referer' ) ) );
		if ( ! valet_central()->is_domain_match_application_password_uuid( $site_domain, $request ) ) {
			return new WP_Error( '401', "Application Password and request from domain don't match each other." );
		}
		$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

		$item = $this->prepare_item_for_note_database( $request, $maintained_site_info->id );
		$data = $this->insert_update_note( $item );
		if ( $data ) {
			return $data;
		}
		return new WP_Error( 'cant-create-or-update', __( 'Note can\'t be created or updated!', 'valet-support' ), array( 'status' => 500 ) );
	}

	/**
	 * Get Valets table name
	 *
	 * @return string valet notes table name
	 */
	private function get_note_table_name() {
		return $GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_NOTE;
	}
	/**
	 * Insert or update data in table
	 */
	private function insert_update_note( $data = array() ) {

		// Update
		if ( isset( $data['id'] ) ) {
			$ret = $GLOBALS['wpdb']->update(
				$this->get_note_table_name(),
				array(
					'maintained_site_id' => $data['maintained_site_id'],
					'note'        => $data['note'],
					'update_date' => current_time( 'mysql', true ),
				),
				array(
					'id' => $data['id'],
				),
				array(
					'%d',
					'%s',
					'%s',
				),
				array(
					'%d',
				)
			);

			if ( is_wp_error( $ret ) ) {
				return $ret;
			}

			if ( false === $ret ) {
				return new WP_Error( 'cant-update-note', __( 'Note can\'t be updated!', 'valet-support' ) );
			}

			$sql  = $GLOBALS['wpdb']->prepare( 'SELECT id, note, create_date FROM ' . $this->get_note_table_name() . ' WHERE id=%d;', intval( $request['id'] ) );
			$data = $GLOBALS['wpdb']->get_row( $sql );

			return $data;

		} else { // Insert

			$data = array(
				'maintained_site_id' => $data['maintained_site_id'],
				'note'        => $data['note'],
				'create_date' => current_time( 'mysql', true ),
			);
			$ret  = $GLOBALS['wpdb']->insert(
				$this->get_note_table_name(),
				$data,
				array(
					'%d',
					'%s',
					'%s',
				)
			);

			if ( $ret ) {
				$data['id'] = $GLOBALS['wpdb']->insert_id;
				return $data;
			} elseif ( is_wp_error( $ret ) ) {
				return $ret;
			} elseif ( false === $ret ) {
				return new WP_Error( 'cant-create-note', __( 'Note can\'t be created!', 'valet-support' ) );
			}
		}
	}

	/**
	 * Delete one item from the collection
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function delete_item( $request ) {
		$site_domain = valet_central()->get_domain_from_url( sanitize_text_field( $request->get_header( 'referer' ) ) );
		if ( ! valet_central()->is_domain_match_application_password_uuid( $site_domain, $request ) ) {
			return new WP_Error( '401', "Application Password and request from domain don't match each other." );
		}
		$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

		$sql    = $GLOBALS['wpdb']->prepare( 'SELECT count( id ) FROM ' . $this->get_note_table_name() . ' WHERE id=%d AND maintained_site_id=%d LIMIT 1;', intval( $request['id'] ), intval( $maintained_site_info->id ) );
		$exists = $GLOBALS['wpdb']->get_var( $sql );
		unset( $sql );

		if ( $exists ) {
			$sql = $GLOBALS['wpdb']->prepare( 'DELETE FROM ' . $this->get_note_table_name() . ' WHERE id=%d AND maintained_site_id=%d LIMIT 1;', intval( $request['id'] ), intval( $maintained_site_info->id )  );
			$ret = $GLOBALS['wpdb']->query( $sql );

			unset( $sql );
			if ( $ret ) {
				return true;
			} else {
				return new WP_Error( 'cant-delete', __( 'A note can\'t be deleted!', 'valet-support' ) );
			}
		} else {
			return new WP_Error( 'cant-find', __( 'A note can\'t be found!', 'valet-support' ) );
		}
	}

	/**
	 * Update one item from the collection
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function update_item( $request ) {
		$site_domain = valet_central()->get_domain_from_url( sanitize_text_field( $request->get_header( 'referer' ) ) );
		if ( ! valet_central()->is_domain_match_application_password_uuid( $site_domain, $request ) ) {
			return new WP_Error( '401', "Application Password and request from domain don't match each other." );
		}
		$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

		$sql    = $GLOBALS['wpdb']->prepare( 'SELECT count( id ) FROM ' . $this->get_note_table_name() . ' WHERE id=%d AND maintained_site_id=%d LIMIT 1;', intval( $request['id'] ), intval( $maintained_site_info->id ) );
		$exists = $GLOBALS['wpdb']->get_var( $sql );

		unset( $sql );

		if ( $exists ) {
			$note = sanitize_textarea_field( $request['note'] );

			$ret = $GLOBALS['wpdb']->update(
				$this->get_note_table_name(),
				array( 'note' => $request['note'] ),
				array(
					'id' => intval( $request['id'] ),
					'maintained_site_id' => intval( $maintained_site_info->id ),
				),
				array( '%s' ),
				array(
					'%d',
					'%d'
				)
			);
			if ( $ret ) {
				return true;
			} else {
				// May be user has given same note, so we should not give any error
				$sql           = $GLOBALS['wpdb']->prepare( 'SELECT note FROM ' . $this->get_note_table_name() . ' WHERE id=%d AND maintained_site_id=%d LIMIT 1;', intval( $request['id'] ), intval( $maintained_site_info->id ) );
				$existing_note = $GLOBALS['wpdb']->get_var( $sql );
				if ( $request['note'] == $existing_note ) {
					return true;
				}
				return new WP_Error( 'cant-update', __( 'A note can\'t be updated!', 'valet-support' ) );
			}
		} else {
			return new WP_Error( 'cant-find', __( 'A note can\'t be found!', 'valet-support' ) );
		}
	}


	/**
	 * Prepare the item for create or update operation
	 *
	 * @param WP_REST_Request $request Request object
	 * @return WP_Error|object $prepared_item
	 */
	protected function prepare_item_for_note_database( $request, $maintained_site_id ) {
		// here write logic to prepare data
		$ret_data = array(
			'note' => sanitize_textarea_field( $request['note'] ),
			'maintained_site_id' => intval( $maintained_site_id ),
		);

		if ( isset( $request['id'] ) ) {
			$ret_data['id'] = absint( $request['id'] );
		}
		return $ret_data;
	}
}
