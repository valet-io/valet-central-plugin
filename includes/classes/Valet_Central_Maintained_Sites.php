<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Valet_Central_Maintained_Sites {

	public function __construct() {

	}

	public function run() {
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ], 10, 1 );
		add_action( 'wp_create_application_password', [ $this, 'wp_create_application_password' ], 10, 4 );
	}

	public function admin_menu() {
		add_menu_page(
			__( 'Valet Central', 'valet-central' ),
			__( 'Valet Central', 'valet-central' ),
			'manage_options',
			'valet-central',
			[ $this, 'render_page' ],
			'dashicons-star-filled',
			2
		);
	}

	public function render_page() {
		?>
		<div id="valet-central-maintained-site"></div>
		<?php
	}

	/**
     * Enqueue maintained site scripts
     */
    public function admin_enqueue_scripts( $hook_suffix ) {
		if ( 'toplevel_page_valet-central' !== $hook_suffix ) {
            return;
        }

        $asset_file = include plugin_dir_path( Valet_Central_Main::PLUGIN_FILE ) . '/assets//build/index.asset.php';

        wp_enqueue_script(
            'valet-maintained-sites-admin-js',
            plugin_dir_url( Valet_Central_Main::PLUGIN_FILE ) . 'assets/build/index.js',
            $asset_file['dependencies'],
            $asset_file['version'],
            true
        );
        wp_localize_script(
            'valet-maintained-sites-admin-js',
            'valet_maintained_sites',
            array(
                'route_base' => Valet_Central_Main::NAMESPACE . '/maintained-sites',
            )
        );

        wp_enqueue_style( 'wp-components' );
        wp_enqueue_style(
            'valet-maintained-sites-admin-style',
            plugin_dir_url( Valet_Central_Main::PLUGIN_FILE ) . 'assets/build/index.css',
            array(),
            ( defined( 'WP_DEBUG' ) && WP_DEBUG ) ? time() : Valet_Central_Main::VERSION,
            'all'
        );
    }

	public function wp_create_application_password( $user_id, $new_item, $new_password, $args ) {

		// bail if direct application password created
		if ( ! isset( $args['name'] ) ||  ! isset( $args['app_id'] ) ) {
			return;
		}

		/*
		Array
		(
			[0] => 1
			[1] => Array // $new_item
				(
					[uuid] => f26fcc73-96a4-474d-8320-1fc5fc1097e4
					[app_id] => 28d1b049-c400-4b92-aca8-e6c6ee5c1e2a
					[name] => ValetSupportMUSite - valetsupportmusite.local
					[password] => $P$BUZQF5aP9vq80fzatBRr3zoBgym9o/.
					[created] => 1628707748
					[last_used] => 
					[last_ip] => 
				)

			[2] => SRRSRbLAZo2WSzVywHRjg5aN // $new_password
			[3] => Array
				(
					[name] => ValetSupportMUSite - valetsupportmusite.local
					[app_id] => 28d1b049-c400-4b92-aca8-e6c6ee5c1e2a
				)
		)
		*/
		$app_name = $args['name'];
		$app_id	  = $args['app_id'];

		$uuid	  = $new_item['uuid'];
		$password = $new_item['password'];

		$app_names_parts = array_map( 'trim', explode( ',', $app_name ) );
		$site_name_url = array_map( 'trim', explode( '-', $app_names_parts[0] ) );
		$site_url = filter_var( $site_name_url[1], FILTER_VALIDATE_URL ) ?  : '';

		$site_domain = valet_central()->get_domain_from_url( $site_url );
		$site_name = $site_name_url[0];

		$maintained_site_table_name = $GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_MAINTAINED_SITES;

		$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

		if ( ! is_null( $maintained_site_info ) ) {
			$existing_password = WP_Application_Passwords::get_user_application_password(  $maintained_site_info->user_id, $maintained_site_info->uuid );
			$ret = true;
			if ( ! is_null( $existing_password ) ) {
				$ret = WP_Application_Passwords::delete_application_password( $maintained_site_info->user_id, $maintained_site_info->uuid );
			}

			if ( $ret ) {
				$ret_update = $GLOBALS['wpdb']->update(
										$maintained_site_table_name,
										[
											'uuid' 	   => $uuid,
											'user_id'  => $user_id,
											'app_name' => $app_name,
											'app_id'   => $app_id,
											'password' => valet_central()->encryption->encrypt( $password ),
											'domain'   => sanitize_text_field( $site_domain ),
											'url' 	   => sanitize_text_field( $site_url ),
											'name' => sanitize_text_field( $site_name ),
											'extra_info' => json_encode( $new_item ),
											'update_date' =>  current_time( 'mysql', true ),
										],
										[
											'id' => $maintained_site_info->id
										],
										[
											'%s',
											'%d', // user_id
											'%s',
											'%s',
											'%s',
											'%s',
											'%s',
											'%s',
											'%s',
											'%s',
										],
										[
											'%d'
										]
									);
				if ( false ===  $ret_update ) {
					throw new Exception( sprintf( "Can't update maintained site record with ID: %d", $maintained_site_info->id ) );
				}
			} else {
				throw new Exception( sprintf( "Can't delete existing application password. User ID: %1$s, UUID: %2$s", $maintained_site_info->user_id, $maintained_site_info->uuid ) );
			}
		} else {
			$GLOBALS['wpdb']->insert(
				$GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_MAINTAINED_SITES,
				[
					'uuid' 	   => $uuid,
					'user_id'  => $user_id,
					'app_name' => $app_name,
					'app_id'   => $app_id,
					'password' => valet_central()->encryption->encrypt( $password ),
					'domain'   => sanitize_text_field( $site_domain ),
					'url' 	   => sanitize_text_field( $site_url ),
					'name' 	   => sanitize_text_field( $site_name ),
					'extra_info' => json_encode( $new_item ),
					'create_date' =>  current_time( 'mysql', true ),
				],
				[
					'%s',
					'%d', // user_id
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
				]
			);
		}

	}
}