<?php
/**
 * Class Valet_Central_Cron
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Valet_Central_Cron {

	public function run() {
		add_action( 'admin_init', array( $this, 'admin_init' ) );
		add_action( 'valet_cron_hook', array( $this, 'valet_cron_hook' ) );
	}

	public function admin_init() {
		if ( ! wp_next_scheduled( 'valet_cron_hook' ) ) {
			wp_schedule_event( time(), 'twicedaily', 'valet_cron_hook' );
		}
	}

	public function valet_cron_hook() {
		$this->delete_older_log_entries();
		$this->delete_older_generated_pdf_reports();
	}

	private function delete_older_log_entries() {
		$after_date_time = date( 'Y-m-d H:m:s', strtotime( VALET_DELETE_LOGS_AFTER_TIME_TEXT, strtotime( current_time( 'mysql', true ) ) ) );
		$sql = $GLOBALS['wpdb']->prepare(
			'DELETE FROM ' . $GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_ACTIVITY . ' 
					WHERE create_date < %s',
			$after_date_time
		);
		$ret1 = $GLOBALS['wpdb']->query( $sql );

		$sql = $GLOBALS['wpdb']->prepare(
			'DELETE FROM ' . $GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_SPEED . ' 
					WHERE create_date < %s',
			$after_date_time
		);
		$ret2 = $GLOBALS['wpdb']->query( $sql );

		$sql = $GLOBALS['wpdb']->prepare(
			'DELETE FROM ' . $GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_BACKUP . ' 
					WHERE create_date < %s',
			$after_date_time
		);
		$ret3 = $GLOBALS['wpdb']->query( $sql );

		return $ret1 && $ret2 && $ret3;
	}

	private function delete_older_generated_pdf_reports() {
		$report_dir = valet_central()->get_pdf_report_dir_path();

		if ( ! function_exists( 'list_files' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}
		$list_files = list_files( $report_dir, 2 );

		$two_days_ago_timestamp = time() - (2 * DAY_IN_SECONDS);
		foreach ( $list_files as $list_file ) {
			$pdf_file_name = wp_basename( $list_file );
			$timestamp = self::get_timestamp_from_report_filename( $pdf_file_name );
			if ( $timestamp < $two_days_ago_timestamp ) {
				wp_delete_file( $list_file );
			}
		}
	}

	private static function get_timestamp_from_report_filename($filename) {
        $retval = false;
        if ( self::is_str_ends_with( $filename, '_report.pdf' ) ) {
            $pieces      = explode( '_', $filename);
            $piece_count = count($pieces);
            if ($piece_count >= 4) {
                $numeric_index = count($pieces) - 2;
                // Right before the _installer or _archive
                if (is_numeric($pieces[$numeric_index])) {
                    $retval = (float)$pieces[$numeric_index];
                } else {
                    $retval = false;
                }
            } else {
                $retval = false;
            }
        } else {
            $retval = false;
        }

        return $retval;
    }

    private static function is_str_ends_with( $haystack, $needle ) {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }
        return (substr($haystack, -$length) === $needle);
    }
}
