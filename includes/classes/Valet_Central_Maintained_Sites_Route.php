<?php
/**
 * Class Valet_Central_Maintained_Sites_Route
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Valet_Central_Maintained_Sites_Route extends WP_REST_Controller {
 
	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes() {
		$namespace = Valet_Central_Main::NAMESPACE;
		$base = 'maintained-sites';
		register_rest_route( $namespace, '/' . $base, array(
			array(
				'methods'             => WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_items' ),
				'permission_callback' => array( valet_central(), 'check_rest_permission' ),
				'args'                => array(
				),
			)
		) );

		register_rest_route( $namespace, '/' . $base . '/(?P<id>[\d]+)', array(
			array(
				'methods'             => WP_REST_Server::DELETABLE,
				'callback'            => array( $this, 'delete_item' ),
				'permission_callback' => array( valet_central(), 'check_rest_permission' ),
				'args'                => array(
					'force' => array(
					'default' => false,
					),
				),
			),
		) );

	}
 
	private function get_table_name() {
		return $GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_MAINTAINED_SITES;
	}

	/**
	 * Get a collection of items
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
     */
	public function get_items( $request ) {
		$search = sanitize_text_field( $request['search'] );
		$wh = '';
		if ( ! empty( $search ) ) {
			$search_like = '%' . $GLOBALS['wpdb']->esc_like( $search ) . '%';
			$sql = $GLOBALS['wpdb']->prepare( 'SELECT * FROM ' . $this->get_table_name() . ' WHERE name LIKE %s OR url LIKE %s ORDER BY id DESC;', $search_like, $search_like );
		} else {
			$sql = 'SELECT * FROM ' . $this->get_table_name() . ' ORDER BY id DESC;';
		}
		$data = $GLOBALS['wpdb']->get_results( $sql );

		return new WP_REST_Response( $data, 200 );
	}
 
	/**
	 * Delete one item from the collection
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function delete_item( $request ) {
		$maintained_site_id = intval( $request->get_param( 'id' ) );
		if ( 0 === $maintained_site_id ) {
			return new WP_Error( 'invalid-site-id', "Invalid site Id!" );
		}

		$maintained_site_info = valet_central()->get_maintained_site_info_by_id( $maintained_site_id );
		if ( is_null( $maintained_site_info ) ) {
			return new WP_Error( 'invalid-site-id', "Invalid site Id!" );
		}

		$delete_application_password = valet_central()->delete_application_password( $maintained_site_info->user_id, $maintained_site_info->uuid );
		if ( is_wp_error( $delete_application_password ) ) {
			return $delete_application_password;
		}

		$deleted = valet_central()->delete_maintained_site_info( $maintained_site_id );
		if ( $deleted ) {
			return new WP_REST_Response( true, 200 );
		}
		return new WP_Error( 'cant-delete', __( "Can't delete the maintained site!", 'valet-support' ), array( 'status' => 500 ) );
	}
}