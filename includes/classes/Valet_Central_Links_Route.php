<?php
/**
 * Class Valet_Support_Links_Route
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

final class Valet_Central_Links_Route extends WP_REST_Controller {

	/**
	 * REST route namespace
	 *
	 * @var string
	 */
	protected $namespace;

	/**
	 * REST route base
	 *
	 * @var string
	 */
	private $base;

	/**
	 * Constructor of class
	 */
	public function __construct() {
		$this->namespace = Valet_Central_Main::NAMESPACE;
		$this->base      = '/links';
	}

	/**
	 * Get namespace base URI of rest links route
	 *
	 * @return string
	 */
	public function get_namespace_base_url() {
		return $this->namespace . $this->base;
	}

	/**
	 * Register the routes for the objects of the controller.
	 */
	public function register_routes() {
		$routes = array(
			array(
				'methods'  => WP_REST_Server::CREATABLE,
				'callback' => array( $this, 'create_item' ),
			),
			array(
				'methods'  => WP_REST_Server::READABLE,
				'callback' => array( $this, 'get_items' ),
			),
			array(
				'base_postfix' => '/(?P<id>[\d]+)',
				'methods'      => WP_REST_Server::READABLE,
				'callback'     => array( $this, 'get_item' ),
			),
			array(
				'base_postfix' => '/(?P<id>[\d]+)',
				'methods'      => WP_REST_Server::DELETABLE,
				'callback'     => array( $this, 'delete_item' ),
				'args'         => array(
					'id' => array(
						'validate_callback' => function ( $param, $request, $key ) {
							return is_numeric( $param );
						},
					),
				),
			),
			array(
				'base_postfix' => '/(?P<id>[\d]+)',
				'methods'      => WP_REST_Server::EDITABLE,
				'callback'     => array( $this, 'update_item' ),
				'args'         => array(
					'id' => array(
						'validate_callback' => function ( $param, $request, $key ) {
							return is_numeric( $param );
						},
					),
				),
			),
		);

		foreach ( $routes as $route ) {
			$passed_args = array(
				'methods'             => $route['methods'],
				'callback'            => $route['callback'],
				'permission_callback' => array( valet_central(), 'check_rest_permission' ),
			);
			if ( isset( $args['args'] ) ) {
				$passed_args['args'] = $args['args'];
			}
			register_rest_route(
				$this->namespace,
				$this->base . ( empty( $route['base_postfix'] ) ? '' : $route['base_postfix'] ),
				$passed_args
			);

			unset( $passed_args );
		}

		unset( $routes );
		unset( $route );
	}

	/**
	 * Get a collection of items
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_items( $request ) {
		$site_domain = valet_central()->get_domain_from_url( sanitize_text_field( $request->get_header( 'referer' ) ) );
		if ( ! valet_central()->is_domain_match_application_password_uuid( $site_domain, $request ) ) {
			return new WP_Error( '401', "Application Password and request from domain don't match each other." );
		}

		$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

		return $this->get_option_data( $maintained_site_info->id );
	}

	/**
	 * Get a collection of items
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function get_item( $request ) {
		$site_domain = valet_central()->get_domain_from_url( sanitize_text_field( $request->get_header( 'referer' ) ) );
		if ( ! valet_central()->is_domain_match_application_password_uuid( $site_domain, $request ) ) {
			return new WP_Error( '401', "Application Password and request from domain don't match each other." );
		}

		$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

		$opt_val = $this->get_option_data( $maintained_site_info->id );

		$data = $opt_val[ $request['id'] ];
		if ( is_array( $data ) ) {
			return $data;
		}

		return new WP_Error( 'cant-find-link', __( 'The given ID doesn\'t found!', 'valet-suport' ) );
	}

	/**
	 * Create one item from the collection
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function create_item( $request ) {
		$site_domain = valet_central()->get_domain_from_url( sanitize_text_field( $request->get_header( 'referer' ) ) );
		if ( ! valet_central()->is_domain_match_application_password_uuid( $site_domain, $request ) ) {
			return new WP_Error( '401', "Application Password and request from domain don't match each other." );
		}

		$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

		$item = $this->prepare_item_for_database( $request );
		$data = $this->insert_update_link( $maintained_site_info->id, $item );
		if ( $data ) {
			return $data;
		}
		return new WP_Error( 'cant-create-or-update', __( 'Link can\'t be created or updated!', 'valet-support' ), array( 'status' => 500 ) );
	}

	/**
	 * save option value
	 *
	 * @return array
	 */
	private function get_option_data( $maintained_site_id ) {

		$data = get_site_option( valet_central()->get_dynamic_links_option_name( $maintained_site_id ), false );		if ( false === $data ) {
			$data = array();
		}
		$ret_data = array();
		foreach ( $data as $data_key => $data_val ) {
			$ret_data[] = array(
				'id'    => $data_key,
				'url'   => $data_val['url'],
				'title' => $data_val['title'],
			);
		}
		return $ret_data;
	}

	/**
	 * get option value
	 *
	 * @param array $opt_data
	 * @return boolean
	 */
	private function save_option_data( $maintained_site_id, $opt_data ) {
		return update_site_option( valet_central()->get_dynamic_links_option_name( $maintained_site_id ), $opt_data );
	}

	/**
	 * Insert or update data in table
	 */
	private function insert_update_link( $maintained_site_id, $data = array() ) {

		// Update
		if ( isset( $data['id'] ) ) {
			$opt_val                = $this->get_option_data( $maintained_site_id );
			$opt_val[ $data['id'] ] = array(
				'url'   => $data['url'],
				'title' => $data['title'],
			);
			$ret                    = $this->save_option_data( $maintained_site_id, $opt_val );

			if ( false === $ret ) {
				return new WP_Error( 'cant-update-link', __( 'Link can\'t be updated!', 'valet-support' ) );
			}

			return $data;

		} else { // Insert

			$opt_val   = $this->get_option_data( $maintained_site_id );
			$opt_val[] = array(
				'url'   => $data['url'],
				'title' => $data['title'],
			);

			$ret = $this->save_option_data( $maintained_site_id, $opt_val );

			if ( $ret ) {
				$data['id'] = count( $opt_val ) - 1;
				return $data;
			} elseif ( false === $ret ) {
				return new WP_Error( 'cant-create-link', __( 'Link can\'t be created!', 'valet-support' ) );
			}
		}
	}

	/**
	 * Delete one item from the collection
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function delete_item( $request ) {
		$site_domain = valet_central()->get_domain_from_url( sanitize_text_field( $request->get_header( 'referer' ) ) );
		if ( ! valet_central()->is_domain_match_application_password_uuid( $site_domain, $request ) ) {
			return new WP_Error( '401', "Application Password and request from domain don't match each other." );
		}
		$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

		$opt_val = $this->get_option_data( $maintained_site_info->id );
		unset( $opt_val[ $request['id'] ] );
		$opt_val = array_values( $opt_val );
		$ret = $this->save_option_data( $maintained_site_info->id, $opt_val );

		if ( $ret ) {
			return true;
		} else {
			return new WP_Error( 'cant-delete', __( 'A link can\'t be deleted!', 'valet-support' ) );
		}
	}

	/**
	 * Update one item from the collection
	 *
	 * @param WP_REST_Request $request Full data about the request.
	 * @return WP_Error|WP_REST_Response
	 */
	public function update_item( $request ) {
		$site_domain = valet_central()->get_domain_from_url( sanitize_text_field( $request->get_header( 'referer' ) ) );
		if ( ! valet_central()->is_domain_match_application_password_uuid( $site_domain, $request ) ) {
			return new WP_Error( '401', "Application Password and request from domain don't match each other." );
		}
		$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

		$opt_val = $this->get_option_data( $maintained_site_info->id );

		$item = $this->prepare_item_for_database( $request );

		$temp_data = [
			'url'   => $item['url'],
			'title' => $item['title'],
		];

		if ( $opt_val[$item['id']]['url'] == $temp_data['url'] && $opt_val[$item['id']]['title'] == $temp_data['title'] ) {
			$ret = true;
		} else {
			$opt_val[ $item['id'] ] = $temp_data;
			$ret                    = $this->save_option_data( $maintained_site_info->id, $opt_val );
		}

		if ( $ret ) {
			return true;
		} else {
			return new WP_Error( 'cant-update', __( 'A link can\'t be updated!', 'valet-support' ) );
		}
	}


	/**
	 * Prepare the item for create or update operation
	 *
	 * @param WP_REST_Request $request Request object
	 * @return WP_Error|object $prepared_item
	 */
	protected function prepare_item_for_database( $request ) {
		// here write logic to prepare data
		$ret_data = array(
			'url'   => sanitize_textarea_field( $request['url'] ),
			'title' => sanitize_textarea_field( $request['title'] ),
		);

		if ( isset( $request['id'] ) ) {
			$ret_data['id'] = absint( $request['id'] );
		}
		return $ret_data;
	}
}
