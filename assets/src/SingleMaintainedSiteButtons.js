import { Component } from '@wordpress/element';
import { ButtonGroup, Button } from '@wordpress/components';

export default class SingleMaintainedSiteButtons extends Component {
	render() {
		const { id } = this.props;
		return (
			<ButtonGroup>
				<Button
					onClick={ () => this.props.handleDeleteMaintainedSite( id ) }
					isPrimary
					isDestructive
					isSmall={ true }
				>
					Delete
				</Button>
			</ButtonGroup>
		);
	}
}
