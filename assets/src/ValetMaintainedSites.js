import { Component, Fragment } from '@wordpress/element';
import apiFetch from '@wordpress/api-fetch';
import { Button } from '@wordpress/components';

import MaintainedSitesList from './MaintainedSitesList';

export default class ValetMaintainedSites extends Component {
	constructor( props ) {
		super( props );
		this.state = {
			errorMessage: '',
			successMessage: '',
			maintainedSites: [],
			isLoading: '',
			total: 0,
			search: '',
		};
	}

	componentDidMount() {
		this.loadMaintainedSites();
	}

	loadMaintainedSites = () => {
		apiFetch( {
			path: valet_maintained_sites.route_base + '?search=' + this.state.search,
			method: 'GET',
		} ).then(
			( res ) => {
				this.setState( {
					maintainedSites: res,
					isLoading: false,
				} );
			},
			( error ) => {
				this.setState( {
					isLoading: false,
				} );
				alert(
					'Error in fetching maintained sites list with the message: ' +
						error.message +
						'(' +
						error.code +
						')'
				);
			}
		);
	};

	handleDeleteMaintainedSite = ( id ) => {
		if ( confirm( 'Are you sure you want to delete this maintained site?' ) ) {
			// alert( valet_maintained_sites.route_base + '/' + id );
			apiFetch( {
				path: valet_maintained_sites.route_base + '/' + id,
				method: 'DELETE',
			} ).then(
				( res ) => {
					if ( true === res ) {
						this.loadMaintainedSites();
					}
				},
				( error ) => {
					alert(
						'Error in deleting the maintained site with the message: ' +
							error.message +
							'(' +
							error.code +
							')'
					);
				}
			);
		}
	}

	handleSearchChange = ( event ) => {
		// console.log( event );
		this.setState(
			{ 
				search: event.target.value
			}
		);
	}

	handleSearchKeyUp = ( event ) => {
		if ( 13 == event.keyCode ) {
			this.loadMaintainedSites();
			event.preventDefault();
		}
	}

	handleSearchClick = ( event ) => {
		event.preventDefault();
		this.loadMaintainedSites();
	}

	handleSearchClearClick = ( event ) => {
		event.preventDefault();
		this.setState(
			{ 
				search: ''
			},
			() => this.loadMaintainedSites()
		);
	}

	render() {
		return (
			<Fragment>
				<h1> Maintained Sites </h1>
				{ false === this.state.isLoading &&
					(
						<Fragment>
							<input type="text" className="valet-central search-input-text" value={ this.state.search } placeholder="Search" onChange={ this.handleSearchChange } onKeyUp={ this.handleSearchKeyUp } size="64" />
							<Button isSecondary className="is-button valet-central search-button" isSmall={ true } onClick={ this.handleSearchClick }>Search</Button>
							<Button className="is-button valet-central search-button" isSmall={ true } isDestructive onClick={ this.handleSearchClearClick }>Clear</Button>
						</Fragment>
					)
				}

				<MaintainedSitesList
					// methods
					handleDeleteMaintainedSite={ this.handleDeleteMaintainedSite }
					loadMaintainedSites={ this.loadMaintainedSites }
					// vars
					maintainedSites={ this.state.maintainedSites }
					search={ this.state.search }
					isLoading={ this.state.isLoading }
				/>
			</Fragment>
		);
	}
}
