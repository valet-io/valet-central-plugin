import { render } from '@wordpress/element';
import ValetMaintainedSites from './ValetMaintainedSites';

render(
	<ValetMaintainedSites />,
	document.getElementById( 'valet-central-maintained-site' )
);
