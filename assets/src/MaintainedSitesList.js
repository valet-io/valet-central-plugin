import { Component, Fragment } from '@wordpress/element';
import { Spinner } from '@wordpress/components';

import SingleMaintainedSite from './SingleMaintainedSite';
import './styles/MaintainedSitesList.css';

export default class MaintainedSitesList extends Component {
	render() {
		const { maintainedSites, isLoading, search } = this.props;
		return (
			<div className="valet-maintained-sites-container">
				{ isLoading && (
					<div className="valet-maintained-sites-loader">
						{ ' ' }
						<Spinner color="blue" size="200" />{ ' ' }
					</div>
				) }
				{ ! maintainedSites.length && false === isLoading && (
					<div className="valet-central no-list">
						{ search && ( <Fragment>No maintained site found!</Fragment> ) }
						{ ! search && ( <Fragment>There is no maintained site connected yet.</Fragment> ) }
					</div> )
				}
				{ maintainedSites.map( ( maintainedSite ) => (
					<SingleMaintainedSite
						{ ...maintainedSite }
						handleDeleteMaintainedSite={ this.props.handleDeleteMaintainedSite }

					/>
				) ) }
			</div>
		);
	}
}
