import { Component, Fragment } from '@wordpress/element';
import moment from 'moment';
import apiFetch from '@wordpress/api-fetch';

import SingleMaintainedSiteButtons from './SingleMaintainedSiteButtons';

export default class SingleMaintainedSite extends Component {
	constructor( props ) {
		super( props );
		this.state = {
			maintainedSite: ''
		};
	}

	static getDerivedStateFromProps( props, state ) {
		return { maintainedSite: props.maintainedSite };
	}

	render() {
		const { id, create_date } = this.props;
		const { maintainedSite, errorMessage } = this.state;
		const moment_local = moment.utc( create_date ).local();

		return (
			<div className="card valet-maintained-site-single-container" key={ id }>
				<p>
					Site: <strong>{ this.props.url } { ' - ' }{ this.props.name }</strong><br />
				</p>
				<p>
					Created at:{ ' ' }
					<strong>
						{ ' ' }
						{ moment_local.format(
							'MMMM Do YYYY, dddd, h:mm:ss a'
						) }{ ' ' }
					</strong>{ ' ' }
					({ moment_local.fromNow() })
				</p>
				<SingleMaintainedSiteButtons
						id={ id }
						handleDeleteMaintainedSite={ this.props.handleDeleteMaintainedSite }
				/>
			</div>
		);
	}
}
