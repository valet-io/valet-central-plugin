<?php
/**
 * Plugin Name:     Valet Central
 * Plugin URI:      https://www.valet.io/
 * Description:     Valet Central Plugin to manage all maintained sites.
 * Author:          Prashant Baldha
 * Author URI:      https://www.valet.io/
 * Text Domain:     valet-central
 * Domain Path:     /languages
 * Version:         1.0.1
 *
 * @package         Valet_Central
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Autoloader
 *
 * @param $class_name
 */
function valet_central_autoloader( $class_name ) {
	if ( false !== strpos( $class_name, 'Valet_Central' ) && ! class_exists( $class_name ) ) {
		$classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . '/includes/classes/';
		$class_file  = $class_name . '.php';
		require_once $classes_dir . $class_file;
	}
}
spl_autoload_register( 'valet_central_autoloader' );

require_once plugin_dir_path( __FILE__ ) . 'libs/plugin-update-checker/plugin-update-checker.php';
$valet_central_plugin_update_checker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/pmbaldha/valet-central',
	__FILE__,
	'valet-central-plugin'
);
$valet_central_plugin_update_checker->setAuthentication( 'SyGw3Bgyr9Q1RkzS-My-' );
$valet_central_plugin_update_checker->setBranch( 'master' );

define( 'VALET_DELETE_LOGS_AFTER_TIME_TEXT', '- 90 days' );
define( 'VALET_CENTRAL_PLUGIN_FILE_PATH', __FILE__ );

class Valet_Central_Main {

	/**
	 * database version
	 */
	const VERSION = '1.0.1';

	/**
	 * plugin version option name.
	 */
	const VERSION_OPT_NAME = 'valet_central_version';

	const NAMESPACE = 'valetcentral/v1';

	const PLUGIN_FILE = __FILE__;

	/**
	 * maintained sites table name
	 */
	const TBL_MAINTAINED_SITES = 'valet_central_maintained_sites';

	/**
	 * notes table name
	 */
	const TBL_NOTE = 'valet_support_notes';

	/**
	 * activity log table name
	 */
	const TBL_ACTIVITY = 'valet_support_activity_log';

	/**
	 * speed log table name
	 */
	const TBL_SPEED = 'valet_support_speed_log';

	/**
	 * backup table name
	 */
	const TBL_BACKUP = 'valet_support_backup_log';

	public $encryption;

	/**
	 * Valet_Support constructor.
	 *
	 * used to initialize class properties value.
	 */
	public function __construct() {
		$this->encryption = new Valet_Central_Data_Encryption();
	}

	public function run() {
		register_activation_hook( __FILE__, [ $this, 'activation' ] );

		add_action( 'admin_init', [ $this, 'activation' ] );

		add_action( 'plugins_loaded', [ $this, 'plugins_loaded' ] );
		add_action( 'wp_authenticate_application_password_errors', [ $this, 'wp_authenticate_application_password_errors'], 10, 4 );
		add_action( 'rest_api_init', [ $this, 'rest_api_init' ] );
		add_action( 'wp_delete_application_password', [ $this, 'wp_delete_application_password' ], 10, 2 );
		add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ], 10, 1 );

		( new Valet_Central_Maintained_Sites() )->run();
		( new Valet_Central_Cron() )->run();
	}

	/**
	 * Activation hook.
	 */
	public function activation() {
		$database_version = get_site_option( self::VERSION_OPT_NAME, false );

		if ( false === $database_version || version_compare( $database_version, self::VERSION, '<' ) ) {
			$charset_collate = $GLOBALS['wpdb']->get_charset_collate();
			require_once ABSPATH . 'wp-admin/includes/upgrade.php';

			$table_name = $GLOBALS['wpdb']->base_prefix . self::TBL_MAINTAINED_SITES;
			$sql        = "CREATE TABLE IF NOT EXISTS $table_name (
									id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
									uuid varchar(255) NOT NULL,
									user_id bigint(20) unsigned,
									app_id varchar(50) NOT NULL,
									app_name varchar(255) NOT NULL,
									`password` varchar(255) NOT NULL,
									domain varchar(255) UNIQUE,
									`url` text,
									`name` varchar(255) NOT NULL DEFAULT '',
									gmt_offset varchar(20) NOT NULL,
									timezone_string varchar(40) NOT NULL,
									`extra_info` text,
									create_date datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
									update_date datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
									PRIMARY KEY (id)
								) $charset_collate;";
			dbDelta( $sql );

			$table_name = $GLOBALS['wpdb']->base_prefix . self::TBL_NOTE;
			$sql        = "CREATE TABLE IF NOT EXISTS $table_name (
									id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
									maintained_site_id int(10) unsigned NOT NULL,
									note text,
									create_date datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
									update_date datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
									PRIMARY KEY (id),
									KEY maintained_site_id (maintained_site_id),
									KEY id_create_update_date (id, create_date, update_date),
									KEY create_date (create_date),
									KEY update_date (update_date)
								) $charset_collate;";
			dbDelta( $sql );

			$table_name = $GLOBALS['wpdb']->base_prefix . self::TBL_ACTIVITY;
			$sql        = "CREATE TABLE $table_name (
									id bigint(60) NOT NULL AUTO_INCREMENT,
									maintained_site_id int(10) unsigned NOT NULL,
									`action` varchar(255) NOT NULL,
									object_type varchar(255) NOT NULL,
									object_slug varchar(255) NOT NULL,
									object_name varchar(255) NOT NULL,
									object_subtype_from varchar(255) DEFAULT NULL,
									object_subtype_to varchar(255) NOT NULL,
									user_id_of_action_by int(20) NOT NULL,
									display_name_of_action_by varchar(255) NOT NULL DEFAULT '',
									email_address_of_action_by varchar(255) NOT NULL DEFAULT '',
                                    is_multisite int(1) DEFAULT 0,
	                                site_id int(20) DEFAULT 0,
                                    site_name varchar(255) DEFAULT '',
									ip varchar(255) NOT NULL,
									create_date datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
									PRIMARY KEY (id),
									KEY maintained_site_id (maintained_site_id),
									KEY object_type (object_type),
									KEY object_name (object_name),
									KEY create_date (create_date),
									KEY object_type_create_date_index (object_type, create_date)
								) $charset_collate;";
			dbDelta( $sql );

			$table_name = $GLOBALS['wpdb']->base_prefix . self::TBL_SPEED;
			$sql        = "CREATE TABLE $table_name (
									id bigint(60) NOT NULL AUTO_INCREMENT,
									maintained_site_id int(10) unsigned NOT NULL,
									speed_index TEXT,
									other_speed_data LONGTEXT,
									requested_url varchar(255) DEFAULT '',
                                    is_multisite int(1) DEFAULT 0,
	                                site_id int(20) DEFAULT 0,
                                    site_name varchar(255) DEFAULT '',
									create_date datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
									PRIMARY KEY (id),
									KEY maintained_site_id (maintained_site_id),
									KEY create_date (create_date)
								) $charset_collate;";
			dbDelta( $sql );

			$table_name = $GLOBALS['wpdb']->base_prefix . self::TBL_BACKUP;
			$sql        = "CREATE TABLE $table_name (
									id bigint(60) NOT NULL AUTO_INCREMENT,
									backup_date_time datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
									maintained_site_id int(10) unsigned NOT NULL,
	                                create_date datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
									PRIMARY KEY (id),
									KEY maintained_site_id (maintained_site_id),
									KEY backup_date_time (backup_date_time),
									KEY create_date (create_date)
								) $charset_collate;";
			dbDelta( $sql );

			$this->enable_auto_update_plugin();

			update_site_option( self::VERSION_OPT_NAME, self::VERSION );
		}
	}

	public function plugins_loaded() {
		$this->activation();
	}

	private function enable_auto_update_plugin() {
        $option       = 'auto_update_plugins';
        $all_items    = apply_filters( 'all_plugins', get_plugins() );
        $auto_updates = (array) get_site_option( $option, array() );

        $auto_updates[] = plugin_basename( __FILE__ );
        $auto_updates   = array_unique( $auto_updates );

        // Remove items that have been deleted since the site option was last updated.
        $auto_updates = array_intersect( $auto_updates, array_keys( $all_items ) );
        update_site_option( $option, $auto_updates );
    }

	public function wp_authenticate_application_password_errors( $error, $user, $item, $password ) {
		$user_id = $user->ID;
		$uuid = $item['uuid'];
		$app_id = $item['app_id'];

		$maintained_site_table_name = $GLOBALS['wpdb']->base_prefix . self::TBL_MAINTAINED_SITES;
		$sql = $GLOBALS['wpdb']->prepare( 'SELECT count(id) as cnt FROM ' . $maintained_site_table_name . ' WHERE 
														user_id=%d
															AND
														uuid=%s
															AND
														app_id=%s',
													$user_id,
													$uuid,
													$app_id
										);
		$is_exist_app_record = $GLOBALS['wpdb']->get_var( $sql );

		if ( $is_exist_app_record ) {
			return $error;
		}

		$error->add( 'cant-find-maintained-site-record', __( "Maintained site record can't found", 'valet-central' ) );
		return $error;
	}

	public function get_dynamic_links_option_name( $maintainance_site_id ) {
        return 'valet_links_' . $maintainance_site_id;
    }

	public function rest_api_init() {
		register_rest_route( self::NAMESPACE, '/connect', array(
				'methods' => 'GET',
				'callback' => function() {
					return true;
				},
				'permission_callback' => array( valet_central(), 'check_rest_permission' ),
			)
		);

		register_rest_route( self::NAMESPACE, '/disconnect', array(
				'methods' => 'GET',
				'callback' => function( $request ) {
					$site_domain = valet_central()->get_domain_from_url( sanitize_text_field( $request->get_header( 'referer' ) ) );
					if ( ! valet_central()->is_domain_match_application_password_uuid( $site_domain, $request ) ) {
						return new WP_Error( '401', "Application Password and request from domain don't match each other." );
					}
					$maintained_site_info = valet_central()->get_maintained_site_info_by_domain( $site_domain );

					$delete_dynamic_links = true;
					if ( false !== get_site_option( $this->get_dynamic_links_option_name( $maintained_site_info->id ), false ) ) {
						$delete_dynamic_links = delete_site_option( $this->get_dynamic_links_option_name( $maintained_site_info->id ) );
					}
					if ( false === $delete_dynamic_links ) {
						return new WP_Error( 'cant-delete-dynamic-links', "Can't delete dynamic links of the associated site!" );
					}

					$delete_application_password = $this->delete_application_password( $maintained_site_info->user_id, $maintained_site_info->uuid );
					if ( is_wp_error( $delete_application_password ) ) {
						return $delete_application_password;
					}

					return $this->delete_maintained_site_info( $maintained_site_info->id );
				},
				'permission_callback' => array( $this, 'check_rest_permission' ),
			)
		);

		( new Valet_Central_Links_Route() )->register_routes();
		( new Valet_Central_Maintained_Sites_Route() )->register_routes();
		( new Valet_Central_Notes_Route() )->register_routes();
		( new Valet_Central_Activity_Log_Route() )->register_routes();
		( new Valet_Central_Speed_Log_Route() )->register_routes();
		( new Valet_Central_Backup_Log_Route() )->register_routes();
		( new Valet_Central_Report_Route() )->register_routes();
	}

	public function delete_application_password( $user_id, $uuid ) {
		$existing_application_password = WP_Application_Passwords::get_user_application_password( $user_id, $uuid );
		if ( is_null( $existing_application_password ) ) {
			return true;
		}

		remove_action( 'wp_delete_application_password', array( $this, 'wp_delete_application_password' ), 10 );
		$delete_application_password = WP_Application_Passwords::delete_application_password( $user_id, $uuid );
		add_action( 'wp_delete_application_password', array( $this, 'wp_delete_application_password' ), 10, 2 );
		return $delete_application_password;
	}

	public function wp_delete_application_password( $user_id, $item ) {
		$uuid = $item['uuid'];

		$maintained_site_table_name = $GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_MAINTAINED_SITES;
		$sql = $GLOBALS['wpdb']->prepare( 'SELECT id FROM ' . $maintained_site_table_name . ' WHERE user_id=%d AND uuid=%s', $user_id, $uuid );
		$maintained_site_id = $GLOBALS['wpdb']->get_var( $sql );
		return $this->delete_maintained_site_info( $maintained_site_id );
	}

	public function delete_maintained_site_info( $maintained_site_id ) {
		$maintained_site_table_name = $GLOBALS['wpdb']->base_prefix . self::TBL_MAINTAINED_SITES;
		$maintained_site_delete     = $GLOBALS['wpdb']->delete( $maintained_site_table_name, ['id' => $maintained_site_id], ['%d'] );
		if ( false === $maintained_site_delete ) {
			return new WP_Error( 'cant-delete-maintenance-site', "Can't delete maintenance site!" );
		}

		$where 		  = ['maintained_site_id' => $maintained_site_id];
		$where_format = ['%d'];

		$tables_to_delete = [
			Valet_Central_Main::TBL_NOTE,
			Valet_Central_Main::TBL_ACTIVITY,
			Valet_Central_Main::TBL_SPEED,
			Valet_Central_Main::TBL_BACKUP,
		];
		foreach ($tables_to_delete as $table_to_delete) {
			$GLOBALS['wpdb']->delete(
				$GLOBALS['wpdb']->base_prefix . $table_to_delete,
				$where,
				$where_format
			);
		}
		unset( $table_to_delete );

		delete_site_option( 'valet_links_' . $maintained_site_id );

		return true;
	}

	public function check_rest_permission( $request ) {
		return current_user_can( is_multisite() ? 'manage_network_options' : 'manage_options' );
	}

	public function is_domain_match_application_password_uuid( $site_domain, $request ) {
		$maintained_site_info = $this->get_maintained_site_info_by_domain( $site_domain );
		if ( is_null( $maintained_site_info ) ) {
			return false;
		}
		return $maintained_site_info->uuid == $GLOBALS['wp_rest_application_password_uuid'];
	}

	public function get_domain_from_url( $site_url ) {
		$parsed_site_url = parse_url( $site_url );
		$site_domain = $parsed_site_url['host'];
		return $site_domain;
	}

	public function get_maintained_site_info_by_domain( $site_domain ) {
		$cache_key = 'maintained_site_info_by_domain';
		$maintained_site_info = wp_cache_get( $site_domain, $cache_key );

		if ( false == $maintained_site_info ) {
			$maintained_site_table_name = $GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_MAINTAINED_SITES;

			$sql 				  		= $GLOBALS['wpdb']->prepare( 'SELECT * FROM ' . $maintained_site_table_name . ' WHERE domain=%s', $site_domain );
			$maintained_site_info		= $GLOBALS['wpdb']->get_row( $sql );

			wp_cache_set( $site_domain, $maintained_site_info, $cache_key, HOUR_IN_SECONDS );
		}
		return $maintained_site_info;
	}

	public function get_maintained_site_info_by_id( $maintained_site_id ) {
		$cache_key = 'maintained_site_info_by_id';
		$maintained_site_info = wp_cache_get( $maintained_site_id, $cache_key );

		if ( false == $maintained_site_info ) {
			$maintained_site_table_name = $GLOBALS['wpdb']->base_prefix . Valet_Central_Main::TBL_MAINTAINED_SITES;

			$sql 				  		= $GLOBALS['wpdb']->prepare( 'SELECT * FROM ' . $maintained_site_table_name . ' WHERE id=%d', $maintained_site_id );
			$maintained_site_info		= $GLOBALS['wpdb']->get_row( $sql );

			wp_cache_set( $maintained_site_id, $maintained_site_info, $cache_key, HOUR_IN_SECONDS );
		}
		return $maintained_site_info;
	}

	/**
     * Enqueue maintained site scripts
     */
    public function admin_enqueue_scripts( $hook_suffix ) {

		if ( 'authorize-application.php' !== $hook_suffix ) {
            return;
        }

		wp_enqueue_script(
            'valet-authorize-application-js',
            plugin_dir_url( Valet_Central_Main::PLUGIN_FILE ) . 'assets/authorize-application.js',
            [ 'jquery' ],
            1.0,
            true
        );
    }

	public function get_pdf_report_dir_path() {
		$upload_dir = wp_upload_dir();
		return $upload_dir['basedir'] . '/valet-reports/';
	}
}

function valet_central() {
	// Store the instance locally to avoid private static replication
	static $instance = null;

	// Only run these methods if they haven't been ran previously
	if ( null === $instance ) {
		$instance = new Valet_Central_Main();
	}

	// Always return the instance
	return $instance;
}

valet_central()->run();